# **Stream.me**

The general idea of the project is video calls and texting between two people. With the recent events causing many people to stay at home, more people use platforms that offer video calls, those video calls could contain sensitive information that could potentially leak which is why encryption was an important part of this project.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

- Python 3.7 and above.
- PyCrypto
- tkinter
- numpy
- Pillow
- Opencv-python
- pyaudio
- C++ compiler.
- Cryptopp
- Json- https://github.com/nlohmann/json

### Installing

Install the server and the client to separate folders.

### Running
Run the server and after the server is listening, run the client. If the server and the client are in different subnets, you need to use port forwarding for the following ports: 3535, 5555, 4242, 3333.

## Built With
•	tkinter - The GUI framework used

## Authors
•	Idan Shmuel - https://gitlab.com/idan_shmuel

•	Lior baruch - https://gitlab.com/liorb5550

