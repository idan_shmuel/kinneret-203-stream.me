import tkinter.font as tkFont
from Screen import *
import SignInScreen as SignIn
import SignUpScreen as SignUp


class WelcomeScreen(Screen):
    def __init__(self, master):
        # Setting master
        super().__init__(master)
        self.master.title('Welcome')

        # Setting message
        self.topMessage = tk.Label(self.master, text='Welcome', font=tkFont.Font(family='David', size=90))
        self.topMessage.place(relx=0.5, relwidth=1, rely=0.1, anchor='n')

        # Setting the buttons
        self.signInBtn = tk.Button(self.master, text='Log Into Existing Account', bg='light green', command=lambda: self.changeWindow(SignIn.SignInScreen))
        self.signInBtn.place(relx=0.5, rely=0.4, width=ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')

        self.signUpBtn = tk.Button(self.master, text='Dont have an account yet? Register now', bg='light green', command=lambda: self.changeWindow(SignUp.SignUpScreen))
        self.signUpBtn.place(relx=0.5, rely=0.5, width=1.5*ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')
