import sys
import json
import Communicator
import tkinter as tk

# defining sizes
ENTRY_WIDTH = 150
ENTRY_HEIGHT = 40
BUTTON_WIDTH = 50
BUTTON_HEIGHT = 30
BEFORE_WRITE_COLOR = 'silver'
AFTER_WRITE_COLOR = 'black'

# defining codes
EXIT = '000'
SIGN_UP = '001'
SIGN_IN = '002'
GET_USERNAME = '003'
SEND_FRIEND_REQUEST = '004'
SHOW_FRIEND_REQUESTS = '005'
RESPOND_FRIEND_REQUEST = '006'
SHOW_FRIENDS = '007'
REMOVE_FRIEND = '008'
START_VIDEO_CONVERSATION = '009'
JOIN_VIDEO_CONVERSATION = '010'
HANG_UP = '011'
OPEN_PRIVATE_CHAT = '012'
SEND_PRIVATE_MSG = '013'

# setting the connection.
COMMUNICATOR = Communicator.Communicator()
COMMUNICATOR.open_sock()
COMMUNICATOR.openChatSocket()
COMMUNICATOR.keyExchange()

class Screen:
    def __init__(self, master):
        # setting master
        self.master = master
        self.master.state('zoomed')
        self.master.minsize(1100, 600)
        # self.master.overrideredirect(True)
        self.curr_x = 0
        self.curr_y = 0

        self.titleFrame = tk.Frame(self.master, bg='grey')
        self.titleFrame.place(relx=0.5, anchor='n', relwidth=1, height=30)
        # self.titleFrame.bind('<B1-Motion>', lambda event: self.changeWindowSize())
        self.titleFrame.bind('<Double-1>', lambda event: self.restore())
        self.titleFrame.bind("<ButtonPress-1>", self.start_move)
        self.titleFrame.bind("<ButtonRelease-1>", self.stop_move)
        self.titleFrame.bind("<B1-Motion>", self.do_move)

        # Setting the buttons
        self.exitBtn = tk.Button(self.titleFrame, relief='flat', text='❌', bg='grey', command=self.closeWindow)
        self.exitBtn.place(relx=1, width=BUTTON_WIDTH, height=BUTTON_HEIGHT, anchor='ne')

        self.restoreDownBtn = tk.Button(self.titleFrame, relief='flat', text='❐', bg='grey', command=self.restore)
        self.restoreDownBtn.place(relx=1, x=-BUTTON_WIDTH, width=BUTTON_WIDTH, height=BUTTON_HEIGHT, anchor='ne')

        self.minimizeBtn = tk.Button(self.titleFrame, relief='flat', text='―', bg='grey', command=self.minimize)
        self.minimizeBtn.place(relx=1, x=-BUTTON_WIDTH * 2, width=BUTTON_WIDTH, height=BUTTON_HEIGHT, anchor='ne')

        for btn in [self.exitBtn, self.restoreDownBtn, self.minimizeBtn]:
            btn.bind('<Enter>', lambda event, enterBtn=btn: self.enterButton(enterBtn))
            btn.bind('<Leave>', lambda event, leaveBtn=btn: self.leaveButton(leaveBtn))

    # Buttons functions
    def closeWindow(self):
        full_msg = '{"code": "' + EXIT + '"}'
        full_json_msg = json.loads(full_msg)
        COMMUNICATOR.send_chat_message_to_server((str(full_json_msg)).replace("'", '"'))
        COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
        self.master.destroy()
        sys.exit()

    def enterButton(self, enterBtn):
        if enterBtn == self.exitBtn:
            enterBtn['bg'] = 'red'
            enterBtn['fg'] = 'white'
        else:
            enterBtn['bg'] = 'silver'

    @staticmethod
    def leaveButton(leaveBtn):
        leaveBtn['bg'] = 'grey'
        leaveBtn['fg'] = 'black'

    def restore(self):
        if self.master.state() != 'zoomed':
            self.master.state('zoomed')
        else:
            self.master.state('normal')

    def minimize(self):
        self.master.update_idletasks()
        # self.master.overrideredirect(False)
        self.master.state('iconic')

    # frames functions
    def changeWindowSize(self):
        if self.master.state() == 'zoomed':
            self.master.state('normal')

    def start_move(self, event):
        self.curr_x = event.x
        self.curr_y = event.y

    def stop_move(self, event):
        self.curr_x = event.x
        self.curr_y = event.y

    def do_move(self, event):
        delta_x = event.x - self.curr_x
        delta_y = event.y - self.curr_y
        x = self.master.winfo_x() + delta_x
        y = self.master.winfo_y() + delta_y
        self.master.geometry(f'+{x}+{y}')

    def changeWindow(self, target):
        self.master.withdraw()
        target(tk.Toplevel(self.master))  # In order to pass arguments simply write what you want after the first argument

