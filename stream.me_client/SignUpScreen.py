import tkinter.font as tkFont
from Screen import *
import WelcomeScreen as welcome
import re


class SignUpScreen(Screen):
    def __init__(self, master):
        # Setting master
        super().__init__(master)
        self.master.title('SignUp')

        # Setting message
        self.topMessage = tk.Label(self.master, text='Create a New Account', font=tkFont.Font(family='David', size=90))
        self.topMessage.place(relx=0.5, relwidth=1, rely=0.1, anchor='n')

        # setting errors labels
        self.usernameError = tk.Label(self.master, text='', font=tkFont.Font(family='David', size=20), fg='red')
        self.usernameError.place(relx=0.5, x=ENTRY_WIDTH, rely=0.3, anchor='nw')

        self.emailError = tk.Label(self.master, text='', font=tkFont.Font(family='David', size=20), fg='red')
        self.emailError.place(relx=0.5, x=ENTRY_WIDTH, rely=0.4, anchor='nw')

        self.passwordError = tk.Label(self.master, text='', font=tkFont.Font(family='David', size=20), fg='red')
        self.passwordError.place(relx=0.5, x=ENTRY_WIDTH, rely=0.5, anchor='nw')

        self.passwordAuthError = tk.Label(self.master, text='', font=tkFont.Font(family='David', size=20), fg='red')
        self.passwordAuthError.place(relx=0.5, x=ENTRY_WIDTH, rely=0.6, anchor='nw')

        # Setting the entries
        self.usernameVar = tk.StringVar()
        self.usernameVar.set('Enter username')
        self.usernameEntry = tk.Entry(self.master, textvariable=self.usernameVar, fg=BEFORE_WRITE_COLOR,
                                      font=tkFont.Font(family='David', size=15), bg='white', selectforeground=BEFORE_WRITE_COLOR,
                                      selectbackground='white')
        self.usernameEntry.place(relx=0.5, rely=0.3, width=ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')

        self.emailVar = tk.StringVar()
        self.emailVar.set('Enter email')
        self.emailEntry = tk.Entry(self.master, textvariable=self.emailVar, fg=BEFORE_WRITE_COLOR,
                                   font=tkFont.Font(family='David', size=15), bg='white', selectforeground=BEFORE_WRITE_COLOR,
                                   selectbackground='white')
        self.emailEntry.place(relx=0.5, rely=0.4, width=ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')

        self.passwordVar = tk.StringVar()
        self.passwordVar.set('Enter password')
        self.passwordEntry = tk.Entry(self.master, textvariable=self.passwordVar, fg=BEFORE_WRITE_COLOR,
                                      font=tkFont.Font(family='David', size=15), bg='white', selectforeground=BEFORE_WRITE_COLOR,
                                      selectbackground='white')
        self.passwordEntry.place(relx=0.5, rely=0.5, width=ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')

        self.passwordAuthVar = tk.StringVar()
        self.passwordAuthVar.set('Enter password again')
        self.passwordAuthEntry = tk.Entry(self.master, textvariable=self.passwordAuthVar, fg=BEFORE_WRITE_COLOR,
                                          font=tkFont.Font(family='David', size=15), bg='white',
                                          selectforeground=BEFORE_WRITE_COLOR,
                                          selectbackground='white')

        self.passwordAuthEntry.place(relx=0.5, rely=0.6, width=ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')

        for loopEntry, loopVar in zip([self.usernameEntry, self.emailEntry, self.passwordEntry, self.passwordAuthEntry],
                                      [self.usernameVar, self.emailVar, self.passwordVar, self.passwordAuthVar]):
            loopEntry.bind('<Control-x>', lambda event: 'break')  # disable cut
            loopEntry.bind('<Control-c>', lambda event: 'break')  # disable copy
            loopEntry.bind('<Control-v>', lambda event: 'break')  # disable paste
            loopEntry.bind('<Button-3>', lambda event: 'break')  # disable right-click
            loopEntry.bind('<BackSpace>', lambda event: 'break')  # disable Backspace

            loopEntry.bind('<FocusIn>', lambda event, focusInEntry=loopEntry: self.onFocusIn(focusInEntry))
            loopEntry.bind('<FocusOut>', lambda event, focusOutVar=loopVar: self.onFocusOut(focusOutVar))

            # Bind all printable keys
            for i in """0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*(){}[]?_+-=/*.>:;,"'""":
                loopEntry.bind(i, lambda event, _entry=loopEntry, var=loopVar: self.onEntryWrite(_entry, var))

            loopEntry.bind('<space>', lambda event, _entry=loopEntry, var=loopVar: self.onEntryWrite(_entry, var))
            loopEntry.bind('<less>', lambda event, _entry=loopEntry, var=loopVar: self.onEntryWrite(_entry, var))

        # Setting the buttons
        self.submitBtn = tk.Button(self.master, text='Submit', bg='light green', command=lambda: self.onSubmitClicked())
        self.submitBtn.place(relx=0.5, rely=0.7, width=ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')

        self.backBtn = tk.Button(self.master, text='<-', bg='light blue', command=lambda: self.changeWindow(welcome.WelcomeScreen))
        self.backBtn.place(y=30, width=BUTTON_WIDTH, height=BUTTON_HEIGHT)
        self.backBtn.bind('<Enter>', lambda event, enterBtn=self.backBtn: self.enterButton(enterBtn))
        self.backBtn.bind('<Leave>', lambda event, leaveBtn=self.backBtn: self.leaveButton(leaveBtn))

    # entries functions
    def onEntryWrite(self, writeEntry, writeVar):
        writeEntry.bind('<BackSpace>',
                        lambda event, backspaceEntry=writeEntry, backspaceVar=writeVar: self.onBackspaceClicked(
                            backspaceEntry, backspaceVar))
        if writeVar == self.usernameVar and writeEntry['fg'] == BEFORE_WRITE_COLOR:
            writeVar.set('')
            self.usernameEntry['fg'] = AFTER_WRITE_COLOR
        if writeVar == self.emailVar and writeEntry['fg'] == BEFORE_WRITE_COLOR:
            writeVar.set('')
            self.emailEntry['fg'] = AFTER_WRITE_COLOR
        if writeVar == self.passwordVar and writeEntry['fg'] == BEFORE_WRITE_COLOR:
            writeVar.set('')
            self.passwordEntry['fg'] = AFTER_WRITE_COLOR
        if writeVar == self.passwordAuthVar and writeEntry['fg'] == BEFORE_WRITE_COLOR:
            writeVar.set('')
            self.passwordAuthEntry['fg'] = AFTER_WRITE_COLOR

    @staticmethod
    def onFocusIn(focusInEntry):
        if focusInEntry['fg'] == BEFORE_WRITE_COLOR:
            focusInEntry.icursor(0)

    def onFocusOut(self, focusOutVar):
        if focusOutVar.get() == '':
            if focusOutVar == self.usernameVar:
                focusOutVar.set('Enter username')
                self.usernameEntry['fg'] = BEFORE_WRITE_COLOR
            if focusOutVar == self.emailVar:
                focusOutVar.set('Enter email')
                self.emailEntry['fg'] = BEFORE_WRITE_COLOR
            if focusOutVar == self.passwordVar:
                focusOutVar.set('Enter password')
                self.passwordEntry['fg'] = BEFORE_WRITE_COLOR
            if focusOutVar == self.passwordAuthVar:
                focusOutVar.set('Enter password again')
                self.passwordAuthEntry['fg'] = BEFORE_WRITE_COLOR

    def onBackspaceClicked(self, backspaceEntry, backspaceVar):
        self.master.update()
        if len(backspaceVar.get()) == 1 and backspaceEntry.index(tk.INSERT) == 1:
            backspaceEntry.bind('<BackSpace>', lambda e: 'break')
            if backspaceVar == self.usernameVar:
                backspaceVar.set('')
                backspaceVar.set('Enter username')
                self.usernameEntry['fg'] = BEFORE_WRITE_COLOR
            if backspaceVar == self.emailVar:
                backspaceVar.set('')
                backspaceVar.set('Enter email')
                self.emailEntry['fg'] = BEFORE_WRITE_COLOR
            if backspaceVar == self.passwordVar:
                backspaceVar.set('')
                backspaceVar.set('Enter password')
                self.passwordEntry['fg'] = BEFORE_WRITE_COLOR
            if backspaceVar == self.passwordAuthVar:
                backspaceVar.set('')
                backspaceVar.set('Enter password again')
                self.passwordAuthEntry['fg'] = BEFORE_WRITE_COLOR

    def onSubmitClicked(self):
        for label in [self.usernameError, self.emailError, self.passwordError, self.passwordAuthError]:
            label['text'] = ''

        if self.usernameEntry['fg'] == 'silver':
            self.usernameError['text'] = 'this is required!'
        if self.emailEntry['fg'] == 'silver':
            self.emailError['text'] = 'this is required!'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', self.emailVar.get()):
            self.emailError['text'] = 'this is invalid email!'
        if self.passwordEntry['fg'] == 'silver':
            self.passwordError['text'] = 'this is required!'
        elif self.passwordVar.get() != self.passwordAuthVar.get():
            self.passwordAuthError['text'] = 'not the same passwords!'
        else:
            full_msg = '{"code": "' + SIGN_UP + '", "email": "' + self.emailVar.get() + '", "username": "' + self.usernameVar.get() + '", "password": "' + self.passwordVar.get() + '"}'
            full_json_msg = json.loads(full_msg)
            COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
            server_msg = COMMUNICATOR.recv_data_from_server()

            if server_msg != 'OK':
                self.emailError['text'] = server_msg
            else:
                self.changeWindow(welcome.WelcomeScreen)
