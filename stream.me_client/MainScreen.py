import base64
import time
import tkinter.font as tkFont

import pyaudio
from Screen import *
import ChatScreen
import numpy as np
import cv2
from threading import Thread
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from PIL import Image, ImageTk

from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
import random

# setting for audio
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
CHUNK = 4096
BLOCK_SIZE = 16


class MainScreen(Screen):
    def __init__(self, master, email):
        # Setting master
        super().__init__(master)
        self.master.title('Stream.me')
        self.email = email
        self.usernameAndID = self.getUsernameAndID()

        # setting frames
        self.friendsFrame = tk.Frame(self.master, bg='light blue')
        self.friendsFrame.place(anchor='sw', rely=1, relx=0, relwidth=0.2, relheight=0.957)

        self.waitingFriendsRequestsFrame = tk.Frame(self.friendsFrame, bg='light blue')
        self.waitingFriendsRequestsFrame.place(anchor='n', rely=0.20, relx=0.5, relwidth=1, relheight=0.2)

        self.myFriendsFrame = tk.Frame(self.friendsFrame, bg='light blue')
        self.myFriendsFrame.place(anchor='s', rely=0.75, relx=0.5, relwidth=1, relheight=0.2)

        self.videoFrame = tk.Frame(self.master)
        self.videoFrame.place(anchor='sw', rely=1, relx=0.2, relwidth=0.8, relheight=0.957)

        # setting hello message
        self.usernameAndIDLabel = tk.Label(self.friendsFrame, text=self.usernameAndID, bg='light blue',
                                           font=tkFont.Font(family='David', size=20))
        self.usernameAndIDLabel.place(relx=0, rely=1, anchor='sw')

        # setting the entries
        self.friendsSearchVar = tk.StringVar()
        self.friendsSearchVar.set('Enter a Username#0000')
        self.friendsSearchEntry = tk.Entry(self.friendsFrame, textvariable=self.friendsSearchVar, fg=BEFORE_WRITE_COLOR,
                                           font=tkFont.Font(family='David', size=16), bg='white',
                                           selectforeground=BEFORE_WRITE_COLOR,
                                           selectbackground='white')
        self.friendsSearchEntry.place(relx=0.5, rely=0, relwidth=1, height=ENTRY_HEIGHT, anchor='n')

        self.friendsSearchEntry.bind('<Control-x>', lambda event: 'break')  # disable cut
        self.friendsSearchEntry.bind('<Control-c>', lambda event: 'break')  # disable copy
        self.friendsSearchEntry.bind('<Control-v>', lambda event: 'break')  # disable paste
        self.friendsSearchEntry.bind('<Button-3>', lambda event: 'break')  # disable right-click
        self.friendsSearchEntry.bind('<BackSpace>', lambda event: 'break')  # disable Backspace

        self.friendsSearchEntry.bind('<FocusIn>',
                                     lambda event, focusInEntry=self.friendsSearchEntry: self.onFocusIn(focusInEntry))
        self.friendsSearchEntry.bind('<FocusOut>',
                                     lambda event, focusOutVar=self.friendsSearchVar: self.onFocusOut(focusOutVar))

        # Bind all printable keys
        for i in """0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*(){}[]?_+-=/*.>:;,"'""":
            self.friendsSearchEntry.bind(i, lambda event, _entry=self.friendsSearchEntry,
                                                   var=self.friendsSearchVar: self.onEntryWrite(_entry, var))

        self.friendsSearchEntry.bind('<space>', lambda event, _entry=self.friendsSearchEntry,
                                                       var=self.friendsSearchVar: self.onEntryWrite(_entry, var))
        self.friendsSearchEntry.bind('<less>', lambda event, _entry=self.friendsSearchEntry,
                                                      var=self.friendsSearchVar: self.onEntryWrite(_entry, var))

        # Setting the buttons
        self.sendFriendRequestBtn = tk.Button(self.friendsFrame, text='Send Friend Request', bg='light green',
                                              command=lambda: self.searchFriend())
        self.sendFriendRequestBtn.place(relx=0.5, rely=0.14, width=ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')

        self.acceptFriendRequestBtn = tk.Button(self.friendsFrame, text='✓', bg='light green',
                                                command=lambda: self.respondFriendRequest('ACCEPTED'))
        self.acceptFriendRequestBtn.place(relx=1, relwidth=0.5, rely=0.52, height=ENTRY_HEIGHT, anchor='e')

        self.declineFriendRequestBtn = tk.Button(self.friendsFrame, text='✗', bg='light green',
                                                 command=lambda: self.respondFriendRequest('DECLINE'))
        self.declineFriendRequestBtn.place(relwidth=0.5, rely=0.52, height=ENTRY_HEIGHT, anchor='w')

        self.removeFriendBtn = tk.Button(self.friendsFrame, text='Remove Friend', bg='light green',
                                         command=lambda: self.removeFriend())
        self.removeFriendBtn.place(relwidth=1, rely=0.93, height=26.6, anchor='w')

        self.startVideoConversation = tk.Button(self.friendsFrame, text='Start Video Conversation', bg='light green',
                                                command=lambda: self.openConversation())
        self.startVideoConversation.place(relwidth=1, rely=0.89, height=26.6, anchor='w')

        self.joinVideoConversation = tk.Button(self.friendsFrame, text='Join Video Conversation', bg='light green',
                                               command=lambda: self.joinConversation())
        self.joinVideoConversation.place(relwidth=1, rely=0.85, height=26.6, anchor='w')

        self.chooseAvatar = tk.Button(self.master, text='browse logo', bg='yellow', command=lambda: self.browseFile())
        self.chooseAvatar.place(width=100, rely=0, relx=0, height=30, anchor='nw')

        self.refreshFriendFrame = tk.Button(self.master, text='⟳', bg='yellow', command=lambda: self.updateFriendsFrame())
        self.refreshFriendFrame.place(width=100, rely=0, relx=0.073, height=30, anchor='nw')

        # setting messages, labels and scrollbars.
        self.friendsRequestInfo = tk.Message(self.friendsFrame, text='', font=tkFont.Font(family='David', size=16),
                                             fg='red', bg='light blue', width=250)
        self.friendsRequestInfo.place(relx=0.5, relwidth=1, y=ENTRY_HEIGHT, anchor='n')

        self.myFriendRequestScrollbar = tk.Scrollbar(self.waitingFriendsRequestsFrame)
        self.myFriendRequestScrollbar.pack(side=tk.LEFT, fill=tk.Y)

        self.myFriendsScrollbar = tk.Scrollbar(self.myFriendsFrame)
        self.myFriendsScrollbar.pack(side=tk.LEFT, fill=tk.Y)

        self.respondFriendRequestMsg = tk.Message(self.friendsFrame, text='', font=tkFont.Font(family='David', size=16),
                                                  fg='red', bg='light blue', width=250)
        self.respondFriendRequestMsg.place(relx=0.5, relwidth=1, rely=0.41, width=ENTRY_WIDTH, height=ENTRY_HEIGHT,
                                           anchor='n')

        self.bottom_messages = tk.Message(self.friendsFrame, text='', font=tkFont.Font(family='David', size=15),
                                          fg='red', bg='light blue', width=250)
        self.bottom_messages.place(relx=0.5, relwidth=1, rely=0.77, width=ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')

        self.videoLabel = tk.Label(self.videoFrame, bg="white")
        self.selfVideoLabel = tk.Label(self.videoFrame, bg="white")

        # setting list boxes
        self.myRequests = tk.Listbox(self.waitingFriendsRequestsFrame, yscrollcommand=self.myFriendRequestScrollbar.set,
                                     selectmode=tk.SINGLE)
        self.myRequests.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        self.myFriendRequestScrollbar.config(command=self.myRequests.yview)

        self.myFriends = tk.Listbox(self.myFriendsFrame, yscrollcommand=self.myFriendsScrollbar.set)
        self.myFriends.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        self.myFriendsScrollbar.config(command=self.myRequests.yview)

        self.updateFriendsFrame()

        # init the camera for video conversation
        self.camera = None

        # init the microphone for video conversation
        self.input_audio = None
        self.output_audio = None
        self.send_stream = None
        self.recv_stream = None

        # init the buttons of conversation settings
        self.toggleCamera = tk.Button(self.videoFrame, text='camera', bg='light green',
                                      command=lambda: self.manageConversationTools('camera'))

        self.toggleSound = tk.Button(self.videoFrame, text='Mute', bg='light green',
                                     command=lambda: self.manageConversationTools('microphone'))
        self.microphoneOpen = True

        self.hangUpBtn = tk.Button(self.videoFrame, text='✗', bg='light green',
                                   command=lambda: self.manageConversationTools('hangUp'))
        self.inConversation = False

        self.avatarPath = 'stream_me_logo.png'

        self.aesKey = None

        # Bindings
        self.myFriends.bind('<Double-1>', self.openPrivateChat)

        # receiving chat messages
        self.thread_needs_to_be_alive = True
        self.receive_message_thread = Thread(target=self.listen_for_new_chat_message)
        self.receive_message_thread.setDaemon(True)
        self.receive_message_thread.start()

    # entries functions
    def manageConversationTools(self, tool):
        if tool == 'microphone':
            self.microphoneOpen = not self.microphoneOpen
            if self.toggleSound['text'] == 'Mute':
                self.toggleSound['text'] = 'Unmute'
            else:
                self.toggleSound['text'] = 'Mute'
        elif tool == 'hangUp':
            self.inConversation = not self.inConversation
            self.hangUp()
        elif tool == 'camera' and self.camera.isOpened():
            self.camera.release()
        else:
            self.camera = cv2.VideoCapture(0)

    def onEntryWrite(self, writeEntry, writeVar):
        writeEntry.bind('<BackSpace>',
                        lambda event, backspaceEntry=writeEntry, backspaceVar=writeVar: self.onBackspaceClicked(
                            backspaceEntry, backspaceVar))
        if writeVar == self.friendsSearchVar and writeEntry['fg'] == BEFORE_WRITE_COLOR:
            writeVar.set('')
            self.friendsSearchEntry['fg'] = AFTER_WRITE_COLOR

    @staticmethod
    def onFocusIn(focusInEntry):
        if focusInEntry['fg'] == BEFORE_WRITE_COLOR:
            focusInEntry.icursor(0)

    def onFocusOut(self, focusOutVar):
        if focusOutVar.get() == '':
            if focusOutVar == self.friendsSearchVar:
                focusOutVar.set('Enter a Username#0000')
                self.friendsSearchEntry['fg'] = BEFORE_WRITE_COLOR

    def onBackspaceClicked(self, backspaceEntry, backspaceVar):
        self.master.update()
        if len(backspaceVar.get()) == 1 and backspaceEntry.index(tk.INSERT) == 1:
            backspaceEntry.bind('<BackSpace>', lambda e: 'break')
            if backspaceVar == self.friendsSearchVar:
                backspaceVar.set('')
                backspaceVar.set('Enter a Username#0000')
                self.friendsSearchEntry['fg'] = BEFORE_WRITE_COLOR

    def searchFriend(self):
        self.resetLabels()
        self.friendsRequestInfo['fg'] = 'red'
        if self.friendsSearchEntry['fg'] == 'silver':
            self.friendsRequestInfo['text'] = 'this is required!'
        elif '#' not in self.friendsSearchVar.get():
            self.friendsRequestInfo['text'] = 'you have to send the username with his ID!'
        else:
            full_msg = '{"code": "' + SEND_FRIEND_REQUEST + '", "friend1": "' + self.usernameAndID.split('#')[
                0] + '", "ID1": "' + self.usernameAndID.split('#')[1] + '", "friend2": "' + \
                       self.friendsSearchVar.get().split('#')[0] + '", "ID2": "' + \
                       self.friendsSearchVar.get().split('#')[1] + '"}'
            full_json_msg = json.loads(full_msg)
            COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
            server_msg = COMMUNICATOR.recv_data_from_server()

            if server_msg == 'Sent request successfully!':
                self.friendsRequestInfo['fg'] = 'green'
            self.friendsRequestInfo['text'] = server_msg

    def respondFriendRequest(self, response):
        self.resetLabels()

        self.bottom_messages['fg'] = 'red'
        if self.myRequests.curselection():
            if '#' in self.myRequests.get(self.myRequests.curselection()):
                full_msg = '{"code": "' + RESPOND_FRIEND_REQUEST + '", "friend1": "' + self.usernameAndID + '", "friend2": "' + self.myRequests.get(
                    self.myRequests.curselection()) + '", "response": "' + response + '"}'
                full_json_msg = json.loads(full_msg)
                COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
                server_msg = COMMUNICATOR.recv_data_from_server()
                # check if the user accept the request or not.
                if 'and you are now friends!' in server_msg:
                    self.respondFriendRequestMsg['fg'] = 'green'
                self.respondFriendRequestMsg['text'] = server_msg
                self.updateFriendsFrame()

    def getUsernameAndID(self):
        full_msg = '{"code": "' + GET_USERNAME + '", "email": "' + self.email + '"}'
        full_json_msg = json.loads(full_msg)
        COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
        return COMMUNICATOR.recv_data_from_server()

    def updateFriendsFrame(self):
        # show my friend requests
        full_msg = '{"code": "' + SHOW_FRIEND_REQUESTS + '", "usernameAndID": "' + self.usernameAndID + '"}'
        full_json_msg = json.loads(full_msg)
        COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
        server_msg = COMMUNICATOR.recv_data_from_server()

        # setting the scrollbar
        allFriendRequests = server_msg.split(',')
        self.myRequests.delete(0, 'end')

        for request in allFriendRequests:
            self.myRequests.insert(tk.END, str(request))

        # show my friends
        full_msg = '{"code": "' + SHOW_FRIENDS + '", "usernameAndID": "' + self.usernameAndID + '"}'
        full_json_msg = json.loads(full_msg)
        COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
        server_msg = COMMUNICATOR.recv_data_from_server()

        # setting the scrollbar
        allFriends = server_msg.split(',')
        self.myFriends.delete(0, tk.END)
        for friend in allFriends:
            self.myFriends.insert(tk.END, str(friend))

    def removeFriend(self):
        self.resetLabels()
        self.bottom_messages['fg'] = 'red'

        if self.myFriends.curselection() != ():
            if '#' in self.myFriends.get(self.myFriends.curselection()):
                full_msg = '{"code": "' + REMOVE_FRIEND + '", "friend1": "' + self.usernameAndID.split('#')[
                    0] + '", "ID1": "' + self.usernameAndID.split('#')[1] + '", "friend2": "' + \
                           self.myFriends.get(self.myFriends.curselection()).split('#')[0] + '", "ID2": "' + \
                           self.myFriends.get(self.myFriends.curselection()).split('#')[1] + '"}'
                full_json_msg = json.loads(full_msg)
                COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
                server_msg = COMMUNICATOR.recv_data_from_server()
                if server_msg == 'Removed friend successfully!':
                    self.bottom_messages['fg'] = 'green'
                self.bottom_messages['text'] = server_msg
                self.updateFriendsFrame()

    # delete the text from the labels
    def resetLabels(self):
        self.bottom_messages['text'] = ''
        self.respondFriendRequestMsg['text'] = ''
        self.friendsRequestInfo['text'] = ''

    def recv_and_show_frame(self):
        if self.inConversation:
            try:
                frame = COMMUNICATOR.recv_video_from_server()
            except Exception as e:
                print(e)
            else:
                frame = base64.b64decode(frame)
                frame = np.frombuffer(frame, dtype=np.uint8)
                frame = cv2.imdecode(frame, 1)
                image = Image.fromarray(frame)
                imgtk = ImageTk.PhotoImage(image=image)
                self.videoLabel.imgtk = imgtk
                self.videoLabel.configure(image=imgtk)
                self.videoLabel.after(100, self.recv_and_show_frame)

    def send_frame(self):
        while self.inConversation:
            if self.camera.isOpened():
                try:
                    grabbed, frame = self.camera.read()  # grab the current frame
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
                except Exception as e:
                    print(e)
                else:
                    frame = cv2.resize(frame, (300, 300))  # resize the frame
                    encoded, buffer = cv2.imencode('.jpg', frame)
                    jpg_as_text = base64.b64encode(buffer)
                    COMMUNICATOR.send_video_to_server(jpg_as_text)
            else:
                logo = cv2.imread(self.avatarPath, 1)
                logo = cv2.cvtColor(logo, cv2.COLOR_BGR2RGBA)
                logo = cv2.resize(logo, (300, 300))  # resize the frame
                encoded, buffer = cv2.imencode('.jpg', logo)
                jpg_as_text = base64.b64encode(buffer)
                COMMUNICATOR.send_video_to_server(jpg_as_text)

    def callback(self, in_data, frame_count, time_info, status):
        if self.inConversation:
            if self.microphoneOpen:
                COMMUNICATOR.send_audio_to_server(in_data)
            return (None, pyaudio.paContinue)
        return None

    def send_audio(self):
        self.send_stream = self.input_audio.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True,
                                                 frames_per_buffer=CHUNK, stream_callback=self.callback)

    def recv_and_play_audio(self):
        self.recv_stream = self.output_audio.open(format=FORMAT, channels=CHANNELS, rate=RATE, output=True,
                                                  frames_per_buffer=CHUNK)
        while self.inConversation:
            try:
                data = COMMUNICATOR.recv_audio_from_server()
            except Exception as e:
                print(e)
            else:
                self.recv_stream.write(data)

    def show_self(self):
        if self.inConversation:
            if self.camera.isOpened():
                try:
                    grabbed, frame = self.camera.read()  # grab the current frame
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
                except Exception as e:
                    print(e)
                else:
                    frame = cv2.resize(frame, (300, 300))  # resize the frame
                    image = Image.fromarray(frame)
                    imgtk = ImageTk.PhotoImage(image=image)
                    self.selfVideoLabel.imgtk = imgtk
                    self.selfVideoLabel.configure(image=imgtk)
                    self.videoLabel.after(100, self.show_self)

            else:
                logo = cv2.imread(self.avatarPath, 1)
                logo = cv2.cvtColor(logo, cv2.COLOR_BGR2RGBA)
                logo = cv2.resize(logo, (300, 300))  # resize the frame
                image = Image.fromarray(logo)
                imgtk = ImageTk.PhotoImage(image=image)
                self.selfVideoLabel.imgtk = imgtk
                self.selfVideoLabel.configure(image=imgtk)
                self.videoLabel.after(100, self.show_self)

    def openConversation(self):
        self.bottom_messages['text'] = ''

        # start video conversation
        full_msg = '{"code": "' + START_VIDEO_CONVERSATION + '", "usernameAndID": "' + self.usernameAndID + '"}'
        full_json_msg = json.loads(full_msg)
        COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
        self.startConversation()

    def joinConversation(self):
        self.bottom_messages['text'] = ''
        if self.myFriends.curselection():
            if '#' in self.myFriends.get(self.myFriends.curselection()):
                full_msg = '{"code": "' + JOIN_VIDEO_CONVERSATION + '", "usernameAndID": "' + self.usernameAndID + '", "friend": "' + self.myFriends.get(
                    self.myFriends.curselection()) + '"}'
                full_json_msg = json.loads(full_msg)
                COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
                server_msg = COMMUNICATOR.recv_data_from_server()
                if server_msg == 'joined successfully.':
                    self.startConversation()
                else:
                    self.bottom_messages['fg'] = 'black'
                    self.bottom_messages['text'] = server_msg
        else:
            self.bottom_messages['fg'] = 'red'
            self.bottom_messages['text'] = "You didn't choose any user!"

    def startConversation(self):
        COMMUNICATOR.openConversationSockets()
        self.inConversation = True
        self.camera = cv2.VideoCapture(0)
        self.input_audio = pyaudio.PyAudio()
        self.output_audio = pyaudio.PyAudio()
        show_self_video = Thread(target=self.show_self)
        send_video = Thread(target=self.send_frame)
        receive_and_show_video = Thread(target=self.recv_and_show_frame)
        send_audio = Thread(target=self.send_audio)
        receive_and_play_audio = Thread(target=self.recv_and_play_audio)
        show_self_video.setDaemon(True)
        send_video.setDaemon(True)
        receive_and_show_video.setDaemon(True)
        send_audio.setDaemon(True)
        receive_and_play_audio.setDaemon(True)
        show_self_video.start()
        send_video.start()
        receive_and_show_video.start()
        send_audio.start()
        receive_and_play_audio.start()
        self.startVideoConversation.place_forget()
        self.joinVideoConversation.place_forget()
        self.videoLabel.pack(side=tk.LEFT, expand=True)
        self.selfVideoLabel.pack(side=tk.RIGHT, expand=True)
        self.hangUpBtn.place(relx=0.6, width=80, rely=0.8, height=ENTRY_HEIGHT, anchor='s')
        self.toggleCamera.place(relx=0.4, width=80, rely=0.8, height=ENTRY_HEIGHT, anchor='s')
        self.toggleSound.place(relx=0.5, width=80, rely=0.8, height=ENTRY_HEIGHT, anchor='s')

    def hangUp(self):
        full_msg = '{"code": "' + HANG_UP + '"}'
        full_json_msg = json.loads(full_msg)
        COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
        COMMUNICATOR.closeConversationSockets()

        if self.camera.isOpened():
            self.camera.release()

        self.send_stream.stop_stream()
        self.send_stream.close()
        self.recv_stream.close()
        self.input_audio.terminate()
        self.output_audio.terminate()

        # hide conversation buttons, video labels and place the open and join conversation buttons.
        self.hangUpBtn.place_forget()
        self.toggleCamera.place_forget()
        self.toggleSound.place_forget()
        self.selfVideoLabel.pack_forget()
        self.videoLabel.pack_forget()
        self.startVideoConversation.place(relwidth=1, rely=0.89, height=26.6, anchor='w')
        self.joinVideoConversation.place(relwidth=1, rely=0.85, height=26.6, anchor='w')

    def closeWindow(self):
        if self.inConversation:
            self.manageConversationTools('hangUp')
        super().closeWindow()

    def browseFile(self):
        self.avatarPath = filedialog.askopenfilename(initialdir="/", title="Select A File",
                                                     filetype=[('PNG', '*.png'), ('JPEG', '*.jpg')])
        try:
            cv2.cvtColor(cv2.imread(self.avatarPath, 1), cv2.COLOR_BGR2RGBA)
        except Exception:
            tk.messagebox.showwarning("Warning", "Path must include only English letters!!!")
            self.avatarPath = 'stream_me_logo.png'

    def openPrivateChat(self, event):
        if self.myFriends.curselection():
            if '#' in self.myFriends.get(self.myFriends.curselection()):
                full_msg = '{"code": "' + OPEN_PRIVATE_CHAT + '", "usernameAndID": "' + self.usernameAndID + '", "friend": "' + self.myFriends.get(self.myFriends.curselection()) + '"}'
                full_json_msg = json.loads(full_msg)
                COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
                server_msg = COMMUNICATOR.recv_data_from_server()
                if server_msg.split('#')[0] == 'OK':
                    oldChat = ''
                    # get all old chat
                    for i in server_msg.split('#'):
                        oldChat += i + '#'
                    oldChat = oldChat[3:-1]
                    self.changeWindow(ChatScreen.ChatScreen, self.usernameAndID, self.myFriends.get(self.myFriends.curselection()), self.email, oldChat)
                else:
                    tk.messagebox.showwarning("Warning", server_msg)

    def listen_for_new_chat_message(self):
        while self.thread_needs_to_be_alive:
            try:
                COMMUNICATOR.chat_sock.settimeout(0.01)
                message = COMMUNICATOR.recv_chat_message_from_server()
                tk.messagebox.showinfo(message[message.find('-') + 2: message.find(': ')], message[message.find(': ') + 2:])
            except Exception as e:
                pass

    def changeWindow(self, target, usernameAndID, friend, email, oldChat):
        if self.inConversation:
            self.manageConversationTools('hangUp')
        self.thread_needs_to_be_alive = False
        self.master.withdraw()
        target(tk.Toplevel(self.master), usernameAndID, friend, email, oldChat)  # In order to pass arguments simply write what you want after the first argument
