from Screen import *
import MainScreen
from tkinter import messagebox
from datetime import datetime
from threading import Thread


class ChatScreen(Screen):
    def __init__(self, master, usernameAndID, friend, email, oldChat):
        # Setting master
        super().__init__(master)
        self.master.title('Chat')
        self.usernameAndID = usernameAndID
        self.friend = friend
        self.oldChat = oldChat
        self.messagesArea = tk.Text(self.master, state=tk.DISABLED)
        self.messagesArea.place(anchor='se', relx=1, rely=0.952, relwidth=1, relheight=0.909)
        self.textEntry = tk.Entry(self.master)
        self.sendBtn = tk.Button(self.master, text='Send', command=self.send).place(anchor='se', relx=1, rely=1, relwidth=0.2, relheight=0.05)
        self.textEntry.place(anchor='sw', rely=1, relwidth=0.8, relheight=0.05)
        self.backBtn = tk.Button(self.master, text='<-', bg='light blue', command=lambda: self.changeWindow(MainScreen.MainScreen, email))
        self.backBtn.place(y=30, width=BUTTON_WIDTH, height=BUTTON_HEIGHT)
        self.backBtn.bind('<Enter>', lambda event, enterBtn=self.backBtn: self.enterButton(enterBtn))
        self.backBtn.bind('<Leave>', lambda event, leaveBtn=self.backBtn: self.leaveButton(leaveBtn))
        self.loadingOldChat()
        self.currentHour = self.getCurrentHour()
        self.currentDate = self.getCurrentDate()
        self.thread_needs_to_be_alive = True
        self.receive_message_thread = Thread(target=self.recv_message)
        self.receive_message_thread.setDaemon(True)
        self.receive_message_thread.start()


    def send(self):
        message = self.textEntry.get()
        if message == '':
            return
        message_with_date = self.currentDate + ', ' + self.currentHour + ' - ' + self.usernameAndID + ': ' + message
        full_msg = '{"code": "' + SEND_PRIVATE_MSG + '", "usernameAndID": "' + self.usernameAndID + '", "friend": "' + self.friend + '", "message": "' + message_with_date + '"}'
        full_json_msg = json.loads(full_msg)
        COMMUNICATOR.send_chat_message_to_server((str(full_json_msg)).replace("'", '"'))
        self.updateChat(message_with_date)

    def loadingOldChat(self):
        self.messagesArea.config(state=tk.NORMAL)
        self.messagesArea.insert(tk.END, '\n\n\n' + self.friend + '\n')
        self.messagesArea.insert(tk.END, '\n' + self.oldChat.replace('"', '')[:-1])
        self.messagesArea.config(state=tk.DISABLED)

    def recv_message(self):
        while self.thread_needs_to_be_alive:
            try:
                COMMUNICATOR.chat_sock.settimeout(0.01)
                message = COMMUNICATOR.recv_chat_message_from_server()
                senderName = message[message.find('-') + 2: message.find(': ')]
                if senderName == self.friend:
                    self.updateChat(message)
                else:
                    tk.messagebox.showinfo(senderName, message[message.find(': ') + 2:])
            except Exception as e:
                pass

    def updateChat(self, message_with_date):
        self.messagesArea.config(state=tk.NORMAL)
        self.messagesArea.insert(tk.END, '\n' + message_with_date)
        self.messagesArea.config(state=tk.DISABLED)
        self.textEntry.delete(0, tk.END)

    @staticmethod
    def getCurrentHour():
        now = datetime.now()
        return now.strftime("%H:%M")

    @staticmethod
    def getCurrentDate():
        today = datetime.today()
        return today.strftime("%d/%m/%Y")

    def changeWindow(self, target, email):
        self.thread_needs_to_be_alive = False
        self.master.withdraw()
        target(tk.Toplevel(self.master), email)  # In order to pass arguments simply write what you want after the first argument
