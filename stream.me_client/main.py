from WelcomeScreen import WelcomeScreen
import tkinter as tk


def main():
    root = tk.Tk()
    s = WelcomeScreen(root)
    tk.mainloop()


if __name__ == '__main__':
    main()
