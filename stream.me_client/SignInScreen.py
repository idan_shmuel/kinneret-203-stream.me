import tkinter.font as tkFont
from Screen import *
import WelcomeScreen as welcome
import MainScreen


class SignInScreen(Screen):
    def __init__(self, master):
        # Setting master
        super().__init__(master)
        self.master.title('SignIn')

        # Setting message
        self.topMessage = tk.Label(self.master, text='Sign in', font=tkFont.Font(family='David', size=90))
        self.topMessage.place(relx=0.5, relwidth=1, rely=0.1, anchor='n')

        # setting errors labels
        self.messageError = tk.Label(self.master, text='', font=tkFont.Font(family='David', size=20))
        self.messageError.place(relx=0.5, rely=0.3, anchor='n')

        # Setting the entries
        self.emailVar = tk.StringVar()
        self.emailVar.set('Enter email')
        self.emailEntry = tk.Entry(self.master, textvariable=self.emailVar, fg=BEFORE_WRITE_COLOR,
                                   font=tkFont.Font(family='David', size=15), bg='white',
                                   selectforeground=BEFORE_WRITE_COLOR,
                                   selectbackground='white')
        self.emailEntry.place(relx=0.5, rely=0.4, width=ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')

        self.passwordVar = tk.StringVar()
        self.passwordVar.set('Enter password')
        self.passwordEntry = tk.Entry(self.master, textvariable=self.passwordVar, fg=BEFORE_WRITE_COLOR,
                                      font=tkFont.Font(family='David', size=15), bg='white',
                                      selectforeground=BEFORE_WRITE_COLOR,
                                      selectbackground='white')
        self.passwordEntry.place(relx=0.5, rely=0.5, width=ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')

        for loopEntry, loopVar in zip([self.emailEntry, self.passwordEntry],
                                      [self.emailVar, self.passwordVar]):
            loopEntry.bind('<Control-x>', lambda event: 'break')  # disable cut
            loopEntry.bind('<Control-c>', lambda event: 'break')  # disable copy
            loopEntry.bind('<Control-v>', lambda event: 'break')  # disable paste
            loopEntry.bind('<Button-3>', lambda event: 'break')  # disable right-click
            loopEntry.bind('<BackSpace>', lambda event: 'break')  # disable Backspace

            loopEntry.bind('<FocusIn>', lambda event, focusInEntry=loopEntry: self.onFocusIn(focusInEntry))
            loopEntry.bind('<FocusOut>', lambda event, focusOutVar=loopVar: self.onFocusOut(focusOutVar))

            # Bind all printable keys
            for i in """0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*(){}[]?_+-=/*.>:;,"'""":
                loopEntry.bind(i, lambda event, _entry=loopEntry, var=loopVar: self.onEntryWrite(_entry, var))

            loopEntry.bind('<space>', lambda event, _entry=loopEntry, var=loopVar: self.onEntryWrite(_entry, var))
            loopEntry.bind('<less>', lambda event, _entry=loopEntry, var=loopVar: self.onEntryWrite(_entry, var))

        # Setting the buttons
        self.submitBtn = tk.Button(self.master, text='Submit', bg='light green', command=lambda: self.onSubmitClicked())
        self.submitBtn.place(relx=0.5, rely=0.7, width=ENTRY_WIDTH, height=ENTRY_HEIGHT, anchor='n')

        self.backBtn = tk.Button(self.master, text='<-', bg='light blue', command=lambda: super(SignInScreen, self).changeWindow(welcome.WelcomeScreen))
        self.backBtn.place(y=30, width=BUTTON_WIDTH, height=BUTTON_HEIGHT)
        self.backBtn.bind('<Enter>', lambda event, enterBtn=self.backBtn: self.enterButton(enterBtn))
        self.backBtn.bind('<Leave>', lambda event, leaveBtn=self.backBtn: self.leaveButton(leaveBtn))

    # entries functions
    def onEntryWrite(self, writeEntry, writeVar):
        writeEntry.bind('<BackSpace>',
                        lambda event, backspaceEntry=writeEntry, backspaceVar=writeVar: self.onBackspaceClicked(
                            backspaceEntry, backspaceVar))
        if writeVar == self.emailVar and writeEntry['fg'] == BEFORE_WRITE_COLOR:
            writeVar.set('')
            self.emailEntry['fg'] = AFTER_WRITE_COLOR
        if writeVar == self.passwordVar and writeEntry['fg'] == BEFORE_WRITE_COLOR:
            writeVar.set("")
            self.passwordEntry['fg'] = AFTER_WRITE_COLOR

    @staticmethod
    def onFocusIn(focusInEntry):
        if focusInEntry['fg'] == BEFORE_WRITE_COLOR:
            focusInEntry.icursor(0)

    def onFocusOut(self, focusOutVar):
        if focusOutVar.get() == '':
            if focusOutVar == self.emailVar:
                focusOutVar.set('Enter email')
                self.emailEntry['fg'] = BEFORE_WRITE_COLOR
            if focusOutVar == self.passwordVar:
                focusOutVar.set('Enter password')
                self.passwordEntry['fg'] = BEFORE_WRITE_COLOR

    def onBackspaceClicked(self, backspaceEntry, backspaceVar):
        self.master.update()
        if len(backspaceVar.get()) == 1 and backspaceEntry.index(tk.INSERT) == 1:
            backspaceEntry.bind('<BackSpace>', lambda e: 'break')
            if backspaceVar == self.emailVar:
                backspaceVar.set('')
                backspaceVar.set('Enter email')
                self.emailEntry['fg'] = BEFORE_WRITE_COLOR
            if backspaceVar == self.passwordVar:
                backspaceVar.set('')
                backspaceVar.set('Enter password')
                self.passwordEntry['fg'] = BEFORE_WRITE_COLOR

    def onSubmitClicked(self):
        self.messageError['text'] = ''

        if self.emailEntry['fg'] == 'silver' or self.passwordEntry['fg'] == 'silver':
            self.messageError['text'] = 'Email or password are incorrect!'
        else:
            full_msg = '{"code": "' + SIGN_IN + '", "email": "' + self.emailVar.get() + '", "password": "' + self.passwordVar.get() + '"}'
            full_json_msg = json.loads(full_msg)
            COMMUNICATOR.send_data_to_server((str(full_json_msg)).replace("'", '"'))
            server_msg = COMMUNICATOR.recv_data_from_server()
            if server_msg == 'OK':
                self.changeWindow(MainScreen.MainScreen, self.emailVar.get())
            else:
                self.messageError['text'] = server_msg

    def changeWindow(self, target, email):
        self.master.withdraw()
        target(tk.Toplevel(self.master), email)  # In order to pass arguments simply write what you want after the first argument
