import socket
import random
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad

SERVER_IP = '127.0.0.1'
SERVER_PORT = 3535
SERVER_VIDEO_PORT = 4242
SERVER_AUDIO_PORT = 3333
SERVER_CHAT_PORT = 5555
SERVER_ADDRESS = (SERVER_IP, SERVER_PORT)
VIDEO_ADDRESS = (SERVER_IP, SERVER_VIDEO_PORT)
AUDIO_ADDRESS = (SERVER_IP, SERVER_AUDIO_PORT)
CHAT_ADDRESS = (SERVER_IP, SERVER_CHAT_PORT)

class Communicator:
    def __init__(self):
        self.sock = None
        self.video_sock = None
        self.audio_sock = None
        self.chat_sock = None
        self.aesKey = None
        self.exchanged = False

    def open_sock(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        self.sock.connect(SERVER_ADDRESS)

    def openConversationSockets(self):
        self.video_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
        self.video_sock.connect(VIDEO_ADDRESS)
        self.audio_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
        self.audio_sock.connect(AUDIO_ADDRESS)

    def closeConversationSockets(self):
        self.video_sock.close()
        self.audio_sock.close()

    def openChatSocket(self):
        self.chat_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        self.chat_sock.connect(CHAT_ADDRESS)

    def closeChatSocket(self):
        self.chat_sock.close()

    def send_chat_message_to_server(self, msg_to_send):
        self.chat_sock.sendall(self.encrypt(msg_to_send.encode()))

    def recv_chat_message_from_server(self):
        return self.decrypt(self.chat_sock.recv(200000)).decode()

    def send_audio_to_server(self, audio_buffer):
        self.audio_sock.sendto(self.encrypt(audio_buffer), AUDIO_ADDRESS)

    def recv_audio_from_server(self):
        return self.decrypt(self.audio_sock.recvfrom(200000)[0])

    def send_video_to_server(self, frame):
        self.video_sock.sendto(self.encrypt(frame), VIDEO_ADDRESS)

    def recv_video_from_server(self):
        return self.decrypt(self.video_sock.recvfrom(200000)[0])

    def send_data_to_server(self, msg_to_send):
        if self.exchanged:
            self.sock.sendall(self.encrypt(msg_to_send.encode()))
        else:
            self.sock.sendall(msg_to_send.encode())

    def recv_data_from_server(self):
        if self.exchanged:
            return self.decrypt(self.sock.recv(200000)).decode()
        else:
            return self.sock.recv(200000).decode()

    # Encryption and decryption
    def keyExchange(self):
        g = int(self.recv_data_from_server())
        p = int(self.recv_data_from_server())
        serverPubKey = int(self.recv_data_from_server())
        b = random.randint(1000, 10000)
        pubKey = (g ** b) % p
        self.send_data_to_server(str(pubKey))
        self.aesKey = format(((serverPubKey ** b) % p), 'x')
        self.aesKey = self.aesKey.zfill(16)
        self.exchanged = True

    def encrypt(self, data):
        encryptor = AES.new(self.aesKey.encode(), AES.MODE_ECB)
        data = pad(data, 16)
        return encryptor.encrypt(data)

    def decrypt(self, data):
        decrypter = AES.new(self.aesKey.encode(), AES.MODE_ECB)
        return unpad(decrypter.decrypt(data), 16)
