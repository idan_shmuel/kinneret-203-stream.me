#include "SqliteDataBase.h"

int SqliteDataBase::callbackGetUsers(void* data, int argc, char** argv, char** azColName)
{
    std::vector<std::string>* usersList = (std::vector<std::string>*)data;
    for (int i = 0; i < argc; i++)
    {
        if (std::string(azColName[i]) == "email")
        {
            usersList->push_back(argv[i]);
        }
        if (std::string(azColName[i]) == "username")
        {
            usersList->push_back(argv[i]);
        }
        if (std::string(azColName[i]) == "password")
        {
            usersList->push_back(argv[i]);
        }
        if (std::string(azColName[i]) == "id")
        {
            usersList->push_back(std::string(argv[i]));
        }
    }
    return 0;
}

int SqliteDataBase::callbackGetFriends(void* data, int argc, char** argv, char** azColName)
{
    std::vector<std::string>* usersList = (std::vector<std::string>*)data;
    for (int i = 0; i < argc; i++)
    {
        if (std::string(azColName[i]) == "username1")
        {
            usersList->push_back(argv[i]);
        }
        if (std::string(azColName[i]) == "username2")
        {
            usersList->push_back(argv[i]);
        }
        if (std::string(azColName[i]) == "state")
        {
            usersList->push_back(argv[i]);
        }
    }
    return 0;
}

SqliteDataBase::SqliteDataBase()
{
	this->open();
}

SqliteDataBase::~SqliteDataBase()
{
	this->close();
}

void SqliteDataBase::open()
{
    std::string dbFileName = "stream_me_DataBase.sqlite";
    int doesFileExist = _access(dbFileName.c_str(), 0);
    int res = sqlite3_open(dbFileName.c_str(), &this->_db);
    if (res != SQLITE_OK)
    {
        this->_db = nullptr;
        throw std::exception("Failed to open DB");
    }

    if (doesFileExist == -1)
    {
        //create users table
        this->createTable("create table users(id integer not null primary key autoincrement, email text unique not null, username text not null, password text not null);");

        //create friends table
        //insert username#id to the table
        this->createTable("create table friends(id integer not null primary key autoincrement, username1 text not null, username2 text not null, state text not null);");
    }
}

void SqliteDataBase::close()
{
    sqlite3_close(this->_db);
    this->_db = nullptr;
}

void SqliteDataBase::createTable(std::string sqlStatement)
{
    char* errMessage = nullptr;
    int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
    if (res != SQLITE_OK)
    {
        throw std::exception("Failed to create table");
    }
}

bool SqliteDataBase::addUser(SignUp signUp)
{
    std::string sqlStatement = "INSERT INTO USERS (email, username, password) VALUES('" + signUp.email + "', '" + signUp.username + "', '" + signUp.password + "');";
    char* errMessage = nullptr;
    int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
    if (res != SQLITE_OK)
    {
        std::cout << "Failed to add user" << std::endl;
        return false;
    }
    return true;
}

bool SqliteDataBase::doesEmailExist(std::string email)
{
    std::vector<std::string> usersList;
    std::string sqlStatement = "SELECT * FROM USERS WHERE EMAIL = '" + email + "';";
    char* errMessage = nullptr;
    int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetUsers, &usersList, &errMessage);
    if (res != SQLITE_OK)
    {
        std::cout << "Failed to check if user exists" << std::endl;
    }
    return !usersList.empty();
}

bool SqliteDataBase::isPasswordCorrect(SignIn signIn)
{
    if (this->doesEmailExist(signIn.email))
    {
        std::vector<std::string> usersList;
        std::string sqlStatement = "SELECT * FROM USERS WHERE PASSWORD = '" + signIn.password + "' AND EMAIL = '" + signIn.email + "';";
        char* errMessage = nullptr;
        int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetUsers, &usersList, &errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "Failed to check if pasword is correct" << std::endl;
        }
        return !usersList.empty();
    }
    return false;
}

std::string SqliteDataBase::getUsernameByEmail(std::string email)
{
    std::vector<std::string> usersList;
    std::string sqlStatement = "SELECT USERNAME, ID FROM USERS WHERE EMAIL = '" + email + "';";
    char* errMessage = nullptr;
    int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetUsers, &usersList, &errMessage);
    if (res != SQLITE_OK)
    {
        std::cout << "Failed to get the username of this email" << std::endl;
    }
    return usersList.front() + "#" + usersList.back();
}

std::string SqliteDataBase::sendFriendRequest(FriendRequest friendRequest)
{
    if (friendRequest.friend1 + friendRequest.ID1 == friendRequest.friend2 + friendRequest.ID2)
    {
        return "Don't send friend request to yourself!";
    }

    if (this->doesUserExist(friendRequest.friend2, friendRequest.ID2))
    {
        if (!this->doesFriendRequestExist(friendRequest))
        {
            std::string sqlStatement = "INSERT INTO FRIENDS (USERNAME1, USERNAME2, STATE) VALUES('" + friendRequest.friend1 + "#" + friendRequest.ID1 + "', '" + friendRequest.friend2 + "#" + friendRequest.ID2 + "', 'PENDING');";
            char* errMessage = nullptr;
            int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
            if (res != SQLITE_OK)
            {
                std::cout << "Failed to add user" << std::endl;
                return "Failed to send request!";
            }
            return "Sent request successfully!";
        }
        return "There is an active friend request!";
    }
    return "User doesn't exist!";
}

std::string SqliteDataBase::getFriendRequests(std::string user)
{
    std::vector<std::string> usersList;
    std::string sqlStatement = "SELECT USERNAME1 FROM FRIENDS WHERE STATE = 'PENDING' AND USERNAME2 = '" + user + "';";
    char* errMessage = nullptr;
    int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetFriends, &usersList, &errMessage);
    if (res != SQLITE_OK)
    {
        std::cout << "Failed to find user" << std::endl;
        return "Nothing to see here...";
    }
    if (!usersList.empty())
    {
        std::string requests = "";
        for (int i = 0; i < usersList.size(); i++)
        {
            requests += usersList[i];
            if (i != usersList.size() - 1)
            {
                requests += ",";
            }
        }
        return requests;
    }
    return "Nothing to see here...";
}

std::string SqliteDataBase::respondFriendRequest(std::string user1, std::string user2, std::string respond)
{
    if (respond == "ACCEPTED")
    {
        std::string sqlStatement = "UPDATE FRIENDS SET STATE = '" + respond + "' WHERE USERNAME1 = '" + user2 + "' AND USERNAME2 = '" + user1 + "';";
        char* errMessage = nullptr;
        int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "Failed to add user" << std::endl;
            return "Failed to respond request!";
        }
        return user2 + " and you are now friends!";
    }
    else
    {
        std::string sqlStatement = "DELETE FROM FRIENDS WHERE USERNAME1 = '" + user2 + "' AND USERNAME2 = '" + user1 + "';";
        char* errMessage = nullptr;
        int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "Failed to add user" << std::endl;
            return "Failed to respond request!";
        }
        return "This request is no longer active!";
    }

}

std::string SqliteDataBase::getFriends(std::string user)
{
    std::vector<std::string> usersList;
    std::string sqlStatement = "SELECT USERNAME1 FROM FRIENDS WHERE STATE = 'ACCEPTED' AND USERNAME2 = '" + user + "';";
    char* errMessage = nullptr;
    int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetFriends, &usersList, &errMessage);
    if (res != SQLITE_OK)
    {
        std::cout << "Failed to find user" << std::endl;
        return "Nothing to see here...";
    }

    sqlStatement = "SELECT USERNAME2 FROM FRIENDS WHERE STATE = 'ACCEPTED' AND USERNAME1 = '" + user + "';";
    errMessage = nullptr;
    res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetFriends, &usersList, &errMessage);
    if (res != SQLITE_OK)
    {
        std::cout << "Failed to find user" << std::endl;
        return "Nothing to see here...";
    }

    if (!usersList.empty())
    {
        std::string friends = "";
        for (int i = 0; i < usersList.size(); i++)
        {
            friends += usersList[i];
            if (i != usersList.size() - 1)
            {
                friends += ",";
            }
        }
        return friends;
    }
    return "Nothing to see here...";
}

std::string SqliteDataBase::removeFriend(FriendRequest friendRequest)
{
    if (!this->doesFriendRequestExist(friendRequest))
    {
        return "You are not even friends!";
    }
    std::string sqlStatement = "DELETE FROM FRIENDS WHERE USERNAME2 = '" + friendRequest.friend2 + "#" + friendRequest.ID2 + "' AND USERNAME1 = '" + friendRequest.friend1 + "#" + friendRequest.ID1 + "';";
    char* errMessage = nullptr;
    int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetFriends, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Failed to remove friend" << std::endl;
        return "Failed to remove friend";
    }
    sqlStatement = "DELETE FROM FRIENDS WHERE USERNAME2 = '" + friendRequest.friend1 + "#" + friendRequest.ID1 + "' AND USERNAME1 = '" + friendRequest.friend2 + "#" + friendRequest.ID2 + "';";
    errMessage = nullptr;
    res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetFriends, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Failed to remove friend" << std::endl;
        return "Failed to remove friend";
    }
    return "Removed friend successfully!";
}

bool SqliteDataBase::doesFriendRequestExist(FriendRequest friendRequest)
{
    std::vector<std::string> usersList;
    std::string sqlStatement = "SELECT * FROM FRIENDS WHERE USERNAME2 = '" + friendRequest.friend2+"#"+friendRequest.ID2 + "' AND USERNAME1 = '" + friendRequest.friend1+"#"+friendRequest.ID1 + "';";
    char* errMessage = nullptr;
    int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetFriends, &usersList, &errMessage);
    if (res != SQLITE_OK)
    {
        std::cout << "Failed to find friend request" << std::endl;
        return false;
    }
    if (!usersList.empty())
    {
        return true;
    }
    else
    {
        sqlStatement = "SELECT * FROM FRIENDS WHERE USERNAME2 = '" + friendRequest.friend1 + "#" + friendRequest.ID1 + "' AND USERNAME1 = '" + friendRequest.friend2 + "#" + friendRequest.ID2 + "';";
        char* errMessage = nullptr;
        int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetFriends, &usersList, &errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "Failed to find friend request" << std::endl;
            return false;
        }
        if (!usersList.empty())
        {
            return true;
        }
    }
    return false;
}

bool SqliteDataBase::doesUserExist(std::string username, std::string id)
{
    std::vector<std::string> usersList;
    std::string sqlStatement = "SELECT * FROM USERS WHERE USERNAME = '" + username + "' AND ID = '" + id + "';";
    char* errMessage = nullptr;
    int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetUsers, &usersList, &errMessage);
    if (res != SQLITE_OK)
    {
        std::cout << "Failed to find user" << std::endl;
        return false;
    }
    if (!usersList.empty())
    {
        return true;
    }
    return false;
}