#pragma once
#include "pch.h"
#include <aes.h>
#include <osrng.h>
#include <filters.h>
#include <hex.h>
#include <ccm.h>
#include <assert.h>

class AESCryptoAlgorithm
{
public:
	AESCryptoAlgorithm();
	AESCryptoAlgorithm(int aesKey);
	std::string getKey();
	std::string encrypt(std::string data);
	std::string decrypt(std::string data);
private:
	byte key[CryptoPP::AES::DEFAULT_KEYLENGTH];
	std::string PrettyPrint(std::string data);
};