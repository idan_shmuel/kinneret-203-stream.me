#pragma once

#include "WSAInitializer.h"
#include "SqliteDataBase.h"
#include "pch.h"
#include "jsonStructs.h"
#include "Conversation.h"
#include "AESCryptoAlgorithm.h"

class Communicator
{
public:
	Communicator();
	~Communicator();
	void startHandleRequests();

private:
	WSAInitializer wsa;
	std::mutex clientsMutex;
	std::mutex clientSocketsMutex;
	std::mutex clientsNameChatMutex;
	std::mutex clientsKeysMutex;
	std::mutex KeyExchangedMutex;
	SOCKET serverSocket;
	SOCKET chatSocket;
	SOCKET videoSocket;
	SOCKET audioSocket;
	WSAInitializer wsaVideo;
	WSAInitializer wsaAudio;
	// first - general socket, second - ip
	std::map<SOCKET, std::string> clients;
	// first - general socket, second - chat socket
	std::map<SOCKET, SOCKET> clientSockets;
	// first - name, second - chat socket
	std::map<std::string, SOCKET> nameChatClients;
	// first - general socket, second - AES key
	std::map<SOCKET, AESCryptoAlgorithm> clientsKeys;
	// first - general socket, second - is key exchanged
	std::map<SOCKET, bool> isKeyExchanged;

	std::ifstream file;

	std::vector<Conversation*> conversations;
	SqliteDataBase sqliteDataBase;
	void bindAndListen();
	SOCKET accept();
	SOCKET acceptChatSocket();
	void handleClient(SOCKET clientSocket);
	void handleClientChat(SOCKET chatSocket);

	std::string getMyIP(SOCKET clientSocket);
	std::string getNameByChatSocket(SOCKET chatSocket);
	SOCKET getGeneralSocketByChatSocket(SOCKET chatSocket);
	std::string recieveDataFromClient(SOCKET clientSocket);
	void sendDataToClient(SOCKET clientSocket, std::string serverMsg);
	void sendChatMessageToClient(SOCKET chatSocket, std::string serverMsg);
	bool signUp(nlohmann::json clientJsonMsg);
	bool signIn(nlohmann::json clientJsonMsg);

	void receiverVideo();
	void receiverAudio();
	int getPrimeFromFile();

	int keyExchange(SOCKET clientSocket);

	static std::string getSpecificPrimeFromRow(std::string str, char joinC, int col);
	static int getNthSubstr(std::string str, int n, char joinC);
};
