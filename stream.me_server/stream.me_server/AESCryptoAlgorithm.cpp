#include "AESCryptoAlgorithm.h"

AESCryptoAlgorithm::AESCryptoAlgorithm()
{

}

AESCryptoAlgorithm::AESCryptoAlgorithm(int aesKey)
{
	std::stringstream stream;
	stream << std::hex << aesKey;
	std::string result(stream.str());
	result = std::string(16-result.size(), '0').append(result);
	std::copy(result.begin(), result.end(), this->key);
}

std::string AESCryptoAlgorithm::getKey()
{
	std::string keyString;
	keyString.clear();
	CryptoPP::StringSource keyStringSource(this->key, sizeof(this->key), true, new CryptoPP::HexEncoder(new CryptoPP::StringSink(keyString)));
	return keyString;
}

std::string AESCryptoAlgorithm::encrypt(std::string data)
{
	std::string cipher;
	try
	{
		CryptoPP::ECB_Mode<CryptoPP::AES>::Encryption e;
		e.SetKey(this->key, sizeof(this->key));
		CryptoPP::StringSource cipherStringSource(data, true, new CryptoPP::StreamTransformationFilter(e, new CryptoPP::StringSink(cipher)));
	}
	catch (const CryptoPP::Exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	return cipher;
}

std::string AESCryptoAlgorithm::decrypt(std::string data)
{
	std::string recovered;
	try
	{
		CryptoPP::ECB_Mode< CryptoPP::AES >::Decryption d;
		d.SetKey(this->key, sizeof(this->key));
		recovered.clear();
		CryptoPP::StringSource cipherStringSource(data, true, new CryptoPP::StreamTransformationFilter(d, new CryptoPP::StringSink(recovered)));
	}
	catch (const CryptoPP::Exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	return recovered;
}

std::string AESCryptoAlgorithm::PrettyPrint(std::string data)
{
	std::string text;
	text.clear();
	CryptoPP::StringSource source(data, true, new CryptoPP::HexEncoder(new CryptoPP::StringSink(text)));
	return text;
}