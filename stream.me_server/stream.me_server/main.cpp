#include "pch.h"
#include "WSAInitializer.h"
#include "Communicator.h"
#include "AESCryptoAlgorithm.h"

int main(void)
{
	try
	{
		Communicator communicator;
		communicator.startHandleRequests();
	}
	catch (std::exception& e)
	{
		std::cout << "Error: " << e.what() << std::endl;
	}

	return 0;
}