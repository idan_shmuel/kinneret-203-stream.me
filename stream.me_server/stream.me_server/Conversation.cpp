#include "Conversation.h"

Conversation::Conversation(std::string opener, std::string openerIp, SOCKET video_socket, SOCKET audio_socket, int aesKey)
{
	this->videoSocket = video_socket;
	this->audioSocket = audio_socket;

	participantsMutex.lock();
	this->conversationParticipants.push_back(std::pair<std::string, std::string>(opener, openerIp));
	this->participantsKeys.push_back(std::pair<AESCryptoAlgorithm, std::string>(AESCryptoAlgorithm(aesKey), openerIp));
	participantsMutex.unlock();
}

void Conversation::videohandler(std::string ip)
{
	try
	{
		std::map<std::string, sockaddr_in>::iterator it;
		std::string messageBuffer;
		std::string encryptedMsg;
		char client_ip_buffer[INET_ADDRSTRLEN];

		while (this->inConversation(ip))
		{
			this->messageLineMutex.lock();
			for (int i = this->messageLine.size() - 1; i >= 0; i--)
			{
				if (ip == this->messageLine[i].first)
				{
					messageBuffer = this->messageLine[i].second;
					messageBuffer = this->decrypt(ip, messageBuffer);
					this->messageLine.erase(this->messageLine.begin() + i);
				}
			}
			this->messageLineMutex.unlock();
			if (messageBuffer == "")
			{
				continue;
			}

			this->videoMutex.lock();
			for (it = this->videoConversationClients.begin(); it != this->videoConversationClients.end(); it++)
			{
				if (ip == it->first)
				{
					continue;
				}
				encryptedMsg = this->encrypt(it->first, messageBuffer);
				if (sendto(this->videoSocket, encryptedMsg.c_str(), encryptedMsg.size(), 0, (struct sockaddr*) & it->second, sizeof(it->second)) == SOCKET_ERROR)
				{
					printf("sendto() failed with error code : %d", WSAGetLastError());
					exit(EXIT_FAILURE);
				}
			}
			this->videoMutex.unlock();
		}
	}
	catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
	}
}

void Conversation::audiohandler(std::string ip)
{
	try
	{
		std::map<std::string, sockaddr_in>::iterator it;
		std::string chunkBuffer;
		std::string encryptedChunk;
		char client_ip_buffer[INET_ADDRSTRLEN];

		while (this->inConversation(ip))
		{
			this->chunkLineMutex.lock();
			for (int i = this->chunkLine.size() - 1; i >= 0; i--)
			{
				if (ip == this->chunkLine[i].first)
				{
					chunkBuffer = this->chunkLine[i].second;
					chunkBuffer = this->decrypt(ip, chunkBuffer);
					this->chunkLine.erase(this->chunkLine.begin() + i);
				}
			}
			this->chunkLineMutex.unlock();
			if (chunkBuffer == "")
			{
				continue;
			}

			this->audioMutex.lock();
			for (it = this->audioConversationClients.begin(); it != this->audioConversationClients.end(); it++)
			{
				if (ip == it->first)
				{
					continue;
				}
				encryptedChunk = this->encrypt(it->first, chunkBuffer);
 				if (sendto(this->audioSocket, encryptedChunk.c_str(), encryptedChunk.size(), 0, (struct sockaddr*) & it->second, sizeof(it->second)) == SOCKET_ERROR)
				{
					printf("sendto() failed with error code : %d", WSAGetLastError());
					exit(EXIT_FAILURE);
				}
			}
			this->audioMutex.unlock();
		}
	}
	catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
	}
}

void Conversation::joinConversation(std::string newParticipant, std::string newParticipantIp, int aesKey)
{
	this->participantsMutex.lock();
	this->conversationParticipants.push_back(std::pair<std::string, std::string>(newParticipant, newParticipantIp));
	this->participantsKeys.push_back(std::pair<AESCryptoAlgorithm, std::string>(AESCryptoAlgorithm(aesKey), newParticipantIp));
	this->participantsMutex.unlock();
}

std::vector<std::pair<std::string, std::string>> Conversation::getConversationParticipants()
{
	return this->conversationParticipants;
}

std::map<std::string, sockaddr_in> Conversation::getVideoConversationClients()
{
	return this->videoConversationClients;
}

std::map<std::string, sockaddr_in> Conversation::getAudioConversationClients()
{
	return this->audioConversationClients;
}

bool Conversation::inConversation(std::string clientIp)
{
	this->participantsMutex.lock();
	for (int i = 0; i < this->conversationParticipants.size(); i++)
	{
		if (clientIp == this->conversationParticipants[i].second)
		{
			this->participantsMutex.unlock();
			return true;
		}
	}
	this->participantsMutex.unlock();
	return false;
}

void Conversation::addToVideoConversation(std::pair<std::string, sockaddr_in> newParticipant)
{
	this->videoMutex.lock();
	this->videoConversationClients.insert(newParticipant);
	this->videoMutex.unlock();
}

void Conversation::addToAudioConversation(std::pair<std::string, sockaddr_in> newParticipant)
{
	this->audioMutex.lock();
	this->audioConversationClients.insert(newParticipant);
	this->audioMutex.unlock();
}

void Conversation::addToMsgLine(std::pair<std::string, std::string> newMsg)
{
	this->messageLineMutex.lock();
	this->messageLine.push_back(newMsg);
	this->messageLineMutex.unlock();
}

void Conversation::addToChunkLine(std::pair<std::string, std::string> newChunk)
{
	this->chunkLineMutex.lock();
	this->chunkLine.push_back(newChunk);
	this->chunkLineMutex.unlock();
}

void Conversation::leaveConversation(std::string clientIp)
{ 
	for (int i = 0; i < this->conversationParticipants.size(); i++)
	{
		if (this->conversationParticipants[i].second == clientIp)
		{
			this->participantsMutex.lock();
			this->conversationParticipants.erase(this->conversationParticipants.begin() + i);
			this->participantsMutex.unlock();

			this->videoMutex.lock();
			this->videoConversationClients.erase(clientIp);
			this->videoMutex.unlock();

			this->audioMutex.lock();
			this->audioConversationClients.erase(clientIp);
			this->audioMutex.unlock();

			this->participantsKeys.erase(this->participantsKeys.begin() + i);

			break;
		}
	}	
}

std::string Conversation::decrypt(std::string ip, std::string message)
{
	for (int i = 0; i < this->participantsKeys.size(); i++)
	{
		if (this->participantsKeys[i].second == ip)
		{
			return this->participantsKeys[i].first.decrypt(message);
		}
	}
}

std::string Conversation::encrypt(std::string ip, std::string message)
{
	for (int i = 0; i < this->participantsKeys.size(); i++)
	{
		if (this->participantsKeys[i].second == ip)
		{
			return this->participantsKeys[i].first.encrypt(message);
		}
	}
}