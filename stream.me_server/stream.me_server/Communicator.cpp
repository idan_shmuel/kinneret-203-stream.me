#include "Communicator.h"

Communicator::Communicator()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	this->serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
	this->chatSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->chatSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
	if ((this->videoSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == INVALID_SOCKET)
	{
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}
	if ((this->audioSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == INVALID_SOCKET)
	{
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}
	this->file.open("primes50.txt");
}

Communicator::~Communicator()
{
	for (int i = 0; i < this->conversations.size(); i++)
	{
		delete this->conversations[i];
	}
}

void Communicator::startHandleRequests()
{
	bindAndListen();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		SOCKET clientSocket = accept();
		SOCKET clientChatSocket = acceptChatSocket();
		this->clientSocketsMutex.lock();
		this->clientSockets.insert(std::make_pair(clientSocket, clientChatSocket));
		this->clientSocketsMutex.unlock();
		std::thread clientThread(&Communicator::handleClient, this, clientSocket);
		clientThread.detach();
		std::thread clientChatThread(&Communicator::handleClientChat, this, clientChatSocket);
		clientChatThread.detach();
	}
}

void Communicator::bindAndListen()
{
	//UDP
	//VIdeo
	struct sockaddr_in servaddrVideo;

	servaddrVideo.sin_family = AF_INET;
	servaddrVideo.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddrVideo.sin_port = htons(VIDEO_PORT);
	if (bind(this->videoSocket, (SOCKADDR*)&servaddrVideo, sizeof(servaddrVideo)) == SOCKET_ERROR)
	{
		printf("Bind failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}

	//Audio
	struct sockaddr_in servaddrAudio;

	servaddrAudio.sin_family = AF_INET;
	servaddrAudio.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddrAudio.sin_port = htons(AUDIO_PORT);
	if (bind(this->audioSocket, (SOCKADDR*)&servaddrAudio, sizeof(servaddrAudio)) == SOCKET_ERROR)
	{
		printf("Bind failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}

	//TCP
	//Main Session
	struct sockaddr_in socketAddress = { 0 };

	socketAddress.sin_port = htons(PORT); // port that server will listen for
	socketAddress.sin_family = AF_INET;   // must be AF_INET
	socketAddress.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(this->serverSocket, (struct sockaddr*)&socketAddress, sizeof(socketAddress)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}

	// Start listening for incoming requests of clients
	if (listen(this->serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}

	//Chat
	struct sockaddr_in chatSocketAddress = { 0 };

	chatSocketAddress.sin_port = htons(CHAT_PORT); // port that server will listen for
	chatSocketAddress.sin_family = AF_INET;   // must be AF_INET
	chatSocketAddress.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(this->chatSocket, (struct sockaddr*)&chatSocketAddress, sizeof(chatSocketAddress)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}

	// Start listening for incoming requests of clients
	if (listen(this->chatSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
}

SOCKET Communicator::accept()
{
	struct sockaddr_in client_info;
	int size = sizeof(client_info);
	char client_ip_buffer[INET_ADDRSTRLEN];
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(this->serverSocket, (sockaddr*)&client_info, &size);

	if (client_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__);
	}

	inet_ntop(AF_INET, &client_info.sin_addr, client_ip_buffer, sizeof(client_ip_buffer));

	//Converting char* to std::string
	std::string client_ip = client_ip_buffer;

	std::cout << "Client IP: " << client_ip << std::endl;
	std::cout << "Client accepted. Server and client can speak" << std::endl;

	this->clientsMutex.lock();
	this->clients.insert(std::make_pair(client_socket, client_ip));
	this->clientsMutex.unlock();

	return client_socket;
}

SOCKET Communicator::acceptChatSocket()
{
	struct sockaddr_in client_info;
	int size = sizeof(client_info);
	char client_ip_buffer[INET_ADDRSTRLEN];
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_chat_socket = ::accept(this->chatSocket, (sockaddr*)&client_info, &size);

	if (client_chat_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__);
	}

	return client_chat_socket;
}

void Communicator::handleClient(SOCKET clientSocket)
{	
	try
	{
		int aesKey = this->keyExchange(clientSocket);
		this->clientsKeys.insert(std::make_pair(clientSocket, AESCryptoAlgorithm(aesKey)));
		this->isKeyExchanged.insert(std::make_pair(clientSocket, false));
		std::string serverMsg = "";
		while (true)
		{
			std::string clientMsg = this->recieveDataFromClient(clientSocket);
			clientMsg = this->clientsKeys[clientSocket].decrypt(clientMsg);
			nlohmann::json jsonMsg = nlohmann::json::parse(clientMsg);
			if (EXIT != MSG_CODE)
			{	
				if (SIGN_UP == MSG_CODE)
				{
					if (this->signUp(jsonMsg))
					{
						serverMsg = "OK";
					}
					else
					{
						serverMsg = "ERROR: EMAIL ALREADY EXIST.";
					}
					this->sendDataToClient(clientSocket, serverMsg);
				}
				else if (SIGN_IN == MSG_CODE)
				{
					if (this->signIn(jsonMsg))
					{
						serverMsg = "OK";
						// get the chat socket and client ip.
						this->clientsNameChatMutex.lock();
						this->nameChatClients.insert(std::make_pair(this->sqliteDataBase.getUsernameByEmail(jsonMsg["email"]), this->clientSockets[clientSocket]));
						this->clientsNameChatMutex.unlock();
					}
					else
					{
						serverMsg = "ERROR: EMAIL OR PASSWORD ARE INCORRECT.";
					}
					this->sendDataToClient(clientSocket, serverMsg);
				}
				else if (GET_USERNAME == MSG_CODE)
				{
					serverMsg = this->sqliteDataBase.getUsernameByEmail(jsonMsg["email"]);
					this->sendDataToClient(clientSocket, serverMsg);
				}
				else if (SEND_FRIEND_REQUEST == MSG_CODE)
				{
					FriendRequest friendRequest;
					friendRequest.friend1 = jsonMsg["friend1"];
					friendRequest.ID1 = jsonMsg["ID1"];
					friendRequest.friend2 = jsonMsg["friend2"];
					friendRequest.ID2 = jsonMsg["ID2"];
					serverMsg = this->sqliteDataBase.sendFriendRequest(friendRequest);
					this->sendDataToClient(clientSocket, serverMsg);
				}
				else if (SHOW_FRIEND_REQUESTS == MSG_CODE)
				{
					serverMsg = this->sqliteDataBase.getFriendRequests(jsonMsg["usernameAndID"]);
					this->sendDataToClient(clientSocket, serverMsg);
				}
				else if (RESPOND_FRIEND_REQUEST == MSG_CODE)
				{
					serverMsg = this->sqliteDataBase.respondFriendRequest(jsonMsg["friend1"], jsonMsg["friend2"], jsonMsg["response"]);
					this->sendDataToClient(clientSocket, serverMsg);
				}
				else if (SHOW_FRIENDS == MSG_CODE)
				{
					serverMsg = this->sqliteDataBase.getFriends(jsonMsg["usernameAndID"]);
					this->sendDataToClient(clientSocket, serverMsg);
				}
				else if (REMOVE_FRIEND == MSG_CODE)
				{
					FriendRequest friendRequest;
					friendRequest.friend1 = jsonMsg["friend1"];
					friendRequest.ID1 = jsonMsg["ID1"];
					friendRequest.friend2 = jsonMsg["friend2"];
					friendRequest.ID2 = jsonMsg["ID2"];
					serverMsg = this->sqliteDataBase.removeFriend(friendRequest);
					this->sendDataToClient(clientSocket, serverMsg);
				}
				else if (START_VIDEO_CONVERSATION == MSG_CODE)
				{
					Conversation* newConversation = new Conversation(jsonMsg["usernameAndID"], this->getMyIP(clientSocket), this->videoSocket, this->audioSocket, aesKey);
					this->conversations.push_back(newConversation);
					if (this->conversations.size() == 1)
					{
						std::thread receiverVideoThread(&Communicator::receiverVideo, this);
						std::thread receiverAudioThread(&Communicator::receiverAudio, this);
						receiverVideoThread.detach();
						receiverAudioThread.detach();
					}
					std::thread videoThread(&Conversation::videohandler, this->conversations[this->conversations.size() - 1], this->getMyIP(clientSocket));
					std::thread audioThread(&Conversation::audiohandler, this->conversations[this->conversations.size() - 1], this->getMyIP(clientSocket));
					videoThread.detach();
					audioThread.detach();
				}
				else if (JOIN_VIDEO_CONVERSATION == MSG_CODE)
				{
					bool theFriendInConversation = false;
					for (int i = 0; i < this->conversations.size(); i++)
					{
						std::vector<std::pair<std::string, std::string>> conversationParticipants = this->conversations[i]->getConversationParticipants();
						for (int j = 0; j < conversationParticipants.size(); j++)
						{
							if (jsonMsg["friend"] == conversationParticipants[j].first)
							{
								theFriendInConversation = true;
								this->conversations[i]->joinConversation(jsonMsg["usernameAndID"], this->getMyIP(clientSocket), aesKey);
								std::thread videoThread(&Conversation::videohandler, this->conversations[i], this->getMyIP(clientSocket));
								std::thread audioThread(&Conversation::audiohandler, this->conversations[i], this->getMyIP(clientSocket));
								videoThread.detach();
								audioThread.detach();

								// stop the loops.
								i = this->conversations.size();
								break;
							}
						}
					}	
					if (theFriendInConversation)
					{
						serverMsg = "joined successfully.";
						this->sendDataToClient(clientSocket, serverMsg);
					}
					else
					{
						serverMsg = std::string(jsonMsg["friend"]) + " is not in conversation.";
						this->sendDataToClient(clientSocket, serverMsg);
					}
				}
				else if (HANG_UP == MSG_CODE)
				{
					for (int i = 0; i < this->conversations.size(); i++)
					{
						if (this->conversations[i]->inConversation(this->getMyIP(clientSocket)))
						{
							this->conversations[i]->leaveConversation(this->getMyIP(clientSocket));
							// check if someone is in the conversation
							if (this->conversations[i]->getConversationParticipants().size() == 0)
							{
								this->conversations.erase(this->conversations.begin() + i);
							}
							break;
						}
					}
				}
				else if (OPEN_PRIVATE_CHAT == MSG_CODE)
				{
					std::string usernameAndID = jsonMsg["usernameAndID"];
					std::string friendNameAndID = jsonMsg["friend"];
					std::string filename;
					filename = usernameAndID < friendNameAndID ? usernameAndID + "_" + friendNameAndID + ".txt" : friendNameAndID + "_" + usernameAndID + ".txt";
					std::fstream chatFile;

					chatFile.open(filename, std::fstream::in | std::fstream::out | std::fstream::app);
					if (chatFile.is_open())
					{
						std::string oldChat;
						std::string line;
						while (std::getline(chatFile, line))
						{
							oldChat += line + '\n';
						}
						chatFile.close();
						this->sendDataToClient(clientSocket, "OK#" + oldChat);
					}
					else
					{
						this->sendDataToClient(clientSocket, "Can't open chat!");
					}
				}

			}
			else
			{
				//releasing the data of a disconnecting user.
				this->clientSocketsMutex.lock();
				this->clientSockets.erase(clientSocket);
				this->clientSocketsMutex.unlock();
				this->clientsMutex.lock();
				this->clients.erase(clientSocket);
				this->clientsMutex.unlock();
				this->clientsKeysMutex.lock();
				this->clientsKeys.erase(clientSocket);
				this->clientsKeysMutex.unlock();
				this->KeyExchangedMutex.lock();
				this->isKeyExchanged.erase(clientSocket);
				this->KeyExchangedMutex.unlock();
				break;
			}
		}
	}
	catch (std::exception &e)
	{
		//releasing the data of a disconnecting user.
		this->clientSocketsMutex.lock();
		this->clientSockets.erase(clientSocket);
		this->clientSocketsMutex.unlock();
		this->clientsMutex.lock();
		this->clients.erase(clientSocket);
		this->clientsMutex.unlock();
		this->clientsKeysMutex.lock();
		this->clientsKeys.erase(clientSocket);
		this->clientsKeysMutex.unlock();
		this->KeyExchangedMutex.lock();
		this->isKeyExchanged.erase(clientSocket);
		this->KeyExchangedMutex.unlock();
		std::cout << e.what() << std::endl;
	}
}

void Communicator::handleClientChat(SOCKET chatSocket)
{
	std::string server_msg = "";
	while (true)
	{
		try
		{
			std::string clientMsg = this->recieveDataFromClient(chatSocket);
			clientMsg = this->clientsKeys[this->getGeneralSocketByChatSocket(chatSocket)].decrypt(clientMsg);
			nlohmann::json jsonMsg = nlohmann::json::parse(clientMsg);
			if (EXIT != MSG_CODE)
			{
				if (SEND_PRIVATE_MSG == MSG_CODE)
				{
					std::string usernameAndID = jsonMsg["usernameAndID"];
					std::string friendNameAndID = jsonMsg["friend"];
					std::string filename;
					filename = usernameAndID < friendNameAndID ? usernameAndID + "_" + friendNameAndID + ".txt" : friendNameAndID + "_" + usernameAndID + ".txt";
					std::fstream chatFile;
					chatFile.open(filename, std::fstream::in | std::fstream::out | std::fstream::app);
					if (chatFile.is_open())
					{
						chatFile << jsonMsg["message"] << std::endl;
						this->clientsNameChatMutex.lock();
						if (this->nameChatClients.find(friendNameAndID) != this->nameChatClients.end())
						{
							this->sendChatMessageToClient(this->nameChatClients[friendNameAndID], jsonMsg["message"]);
						}
						this->clientsNameChatMutex.unlock();
					}
				}
			}
			else
			{
				//releasing the data of a disconnecting user.
				this->clientsNameChatMutex.lock();
				this->nameChatClients.erase(this->getNameByChatSocket(chatSocket));
				this->clientsNameChatMutex.unlock();
				break;
			}
		}
		catch (std::exception& e)
		{
			//releasing the data of a disconnecting user.
			this->clientsNameChatMutex.lock();
			this->nameChatClients.erase(this->getNameByChatSocket(chatSocket));
			this->clientsNameChatMutex.unlock();
			std::cout << e.what() << std::endl;
			break;
		}
	}
}

std::string Communicator::getMyIP(SOCKET clientSocket)
{
	std::map<SOCKET, std::string>::iterator it;

	for (it = this->clients.begin(); it != this->clients.end(); ++it)
	{
		if (it->first == clientSocket)
		{
			return it->second;
		}
	}
}

std::string Communicator::getNameByChatSocket(SOCKET chatSocket)
{
	std::map<std::string, SOCKET>::iterator it;
	for (it = this->nameChatClients.begin(); it != this->nameChatClients.end(); ++it)
	{
		if (it->second == chatSocket)
		{
			return it->first;
		}
	}
}

std::string Communicator::recieveDataFromClient(SOCKET clientSocket)
{
	char buffer[BUFFER_LENGTH];
	int res = recv(clientSocket, buffer, BUFFER_LENGTH, 0);
	if (res < 0)
	{
		throw std::exception("Failed to recieve message.\n");
	}
	else if(res == 0)
	{
		throw std::exception("Connection closed.\n");
	}

	buffer[res] = '\0';
	return std::string(buffer, res);
}

void Communicator::sendDataToClient(SOCKET clientSocket, std::string serverMsg)
{
	if (this->isKeyExchanged[clientSocket] == true)
	{
		serverMsg = this->clientsKeys[clientSocket].encrypt(serverMsg);
	}

	if (send(clientSocket, serverMsg.c_str(), serverMsg.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

void Communicator::sendChatMessageToClient(SOCKET chatSocket, std::string serverMsg)
{
	if (this->isKeyExchanged[this->getGeneralSocketByChatSocket(chatSocket)] == true)
	{
		serverMsg = this->clientsKeys[this->getGeneralSocketByChatSocket(chatSocket)].encrypt(serverMsg);
	}

	if (send(chatSocket, serverMsg.c_str(), serverMsg.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

bool Communicator::signUp(nlohmann::json clientJsonMsg)
{
	SignUp signUp;
	signUp.email = clientJsonMsg["email"];
	signUp.username = clientJsonMsg["username"];
	signUp.password = clientJsonMsg["password"];
	return(this->sqliteDataBase.addUser(signUp));
}

bool Communicator::signIn(nlohmann::json clientJsonMsg)
{
	SignIn signIn;
	signIn.email = clientJsonMsg["email"];
	signIn.password = clientJsonMsg["password"];
	return(this->sqliteDataBase.isPasswordCorrect(signIn));
}

void Communicator::receiverVideo()
{
	struct sockaddr_in cliaddr;
	memset(&cliaddr, 0, sizeof(cliaddr));
	int len;
	int res;
	char buffer[200000];
	char clientIPBuffer[INET_ADDRSTRLEN];
	//len is value/resuslt
	len = sizeof(cliaddr);
	while (this->conversations.size() > 0)
	{
		res = recvfrom(this->videoSocket, buffer, 200000, 0, (struct sockaddr*) & cliaddr, &len);
		if (res < 0)
		{
			continue;
		}
		buffer[res] = '\0';
		inet_ntop(AF_INET, &cliaddr.sin_addr, clientIPBuffer, sizeof(clientIPBuffer));
		for (int i = 0; i < this->conversations.size(); i++)
		{
			if (this->conversations[i]->inConversation(std::string(clientIPBuffer)))
			{
				if (this->conversations[i]->getConversationParticipants().size() != this->conversations[i]->getVideoConversationClients().size())
				{
					this->conversations[i]->addToVideoConversation(std::pair<std::string, sockaddr_in>(std::string(clientIPBuffer), cliaddr));
				}
				this->conversations[i]->addToMsgLine(std::pair<std::string, std::string>(std::string(clientIPBuffer), std::string(buffer, res)));
				break;
			}
		}
	}
}

void Communicator::receiverAudio()
{
	struct sockaddr_in cliaddr;
	memset(&cliaddr, 0, sizeof(cliaddr));
	int len;
	int res;
	char buffer[200000];
	char clientIPBuffer[INET_ADDRSTRLEN];
	//len is value/resuslt
	len = sizeof(cliaddr);
	while (this->conversations.size() > 0)
	{
		res = recvfrom(this->audioSocket, buffer, 200000, 0, (struct sockaddr*) & cliaddr, &len);
		if (res < 0)
		{
			continue;
		}
		buffer[res] = '\0';
		inet_ntop(AF_INET, &cliaddr.sin_addr, clientIPBuffer, sizeof(clientIPBuffer));
		for (int i = 0; i < this->conversations.size(); i++)
		{
			if (this->conversations[i]->inConversation(std::string(clientIPBuffer)))
			{
				if (this->conversations[i]->getConversationParticipants().size() != this->conversations[i]->getAudioConversationClients().size())
				{
					this->conversations[i]->addToAudioConversation(std::pair<std::string, sockaddr_in>(std::string(clientIPBuffer), cliaddr));
				}
				this->conversations[i]->addToChunkLine(std::pair<std::string, std::string>(std::string(clientIPBuffer), std::string(buffer, res)));
				break;
			}
		}
	}
}

int Communicator::getPrimeFromFile()
{
	srand(time(NULL));
	int numberPlace = rand() % 1000000 + 1;
	int row = numberPlace / 9 + 1;
	int col = numberPlace % 8;
	if (col == 0)
	{
		col = 8;
	}
	std::string requestedRow;
	if (this->file.is_open())
	{
		int line_no = 0;
		while (line_no < row && std::getline(this->file, requestedRow, '\n'))
		{
			line_no++;
		}
		std::string primeNumber = getSpecificPrimeFromRow(requestedRow, ' ', col);
		return std::stoi(primeNumber);
	}
	else
	{
		return -1;
	}
}

std::string Communicator::getSpecificPrimeFromRow(std::string str, char joinC, int col)
{
	std::size_t first = getNthSubstr(str, col-1, joinC);
	std::size_t second = getNthSubstr(str, col, joinC);
	return std::string(&str[first+1], &str[second]);
}

SOCKET Communicator::getGeneralSocketByChatSocket(SOCKET chatSocket)
{
	this->clientsMutex.lock();
	std::map<SOCKET, SOCKET>::iterator it;
	for (it = this->clientSockets.begin(); it != this->clientSockets.end(); ++it)
	{
		if (it->second == chatSocket)
		{
			this->clientsMutex.unlock();
			return it->first;
		}
	}
}

int Communicator::getNthSubstr(std::string str, int n, char joinC)
{
	int i = str.find(joinC);
	for (int j = 0; j < n; j++)
	{
		i = str.find(joinC, i + 1);
	}
	return i;
}

int Communicator::keyExchange(SOCKET clientSocket)
{
	// make a, g and p large random numbers
	std::string tempPubKey;
	std::string privateKey;
	int p = this->getPrimeFromFile();
	//Check what are the correct values for g and a
	int g = rand() % (10000) + 1000;
	int a = rand() % (10000) + 1000;
	std::string call = "python calcPubKey.py " + std::to_string(g) + " " + std::to_string(a) + " " + std::to_string(p);
	system(call.c_str());
	std::fstream calcKey("key.txt");
	std::getline(calcKey, tempPubKey);
	calcKey.close();
	int pubKey = std::stoi(tempPubKey);
	this->sendDataToClient(clientSocket, std::to_string(g));
	Sleep(1);
	this->sendDataToClient(clientSocket, std::to_string(p));
	Sleep(1);
	this->sendDataToClient(clientSocket, std::to_string(pubKey));
	int clientPubKey = std::stoi(this->recieveDataFromClient(clientSocket));
	call = "python calcPubKey.py " + std::to_string(clientPubKey) + " " + std::to_string(a) + " " + std::to_string(p);
	system(call.c_str());
	calcKey.open("key.txt");
	std::getline(calcKey, privateKey);
	calcKey.close();
	remove("key.txt");
	this->isKeyExchanged[clientSocket] = true;
	return std::stoi(privateKey);
}
