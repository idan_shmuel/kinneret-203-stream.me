#pragma once
#include "pch.h"
#include "sqlite3.h"
#include "jsonStructs.h"

class SqliteDataBase
{
public:
	SqliteDataBase();
	~SqliteDataBase();
	void open();
	void close();
	void createTable(std::string sqlStatement);
	bool addUser(SignUp signUp);
	bool doesEmailExist(std::string email);
	bool isPasswordCorrect(SignIn signIn);
	std::string getUsernameByEmail(std::string email);
	std::string sendFriendRequest(FriendRequest friendRequest);
	std::string getFriendRequests(std::string user);
	std::string respondFriendRequest(std::string user1, std::string user2, std::string respond);
	std::string getFriends(std::string user);
	std::string removeFriend(FriendRequest friendRequest);
	bool doesFriendRequestExist(FriendRequest friendRequest);
	bool doesUserExist(std::string username, std::string id);
	static int callbackGetUsers(void* data, int argc, char** argv, char** azColName);
	static int callbackGetFriends(void* data, int argc, char** argv, char** azColName);

private:
	sqlite3* _db;
};

