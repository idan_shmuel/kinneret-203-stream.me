#pragma once
#include "WSAInitializer.h"
#include "pch.h"
#include "AESCryptoAlgorithm.h"

class Conversation
{
public:
	Conversation(std::string opener, std::string openerIp, SOCKET video_socket, SOCKET audio_socket, int aesKey);
	void videohandler(std::string ip);
	void audiohandler(std::string ip);
	void joinConversation(std::string newParticipant, std::string newParticipantIp, int aesKey);
	std::vector<std::pair<std::string, std::string>> getConversationParticipants();
	std::map<std::string, sockaddr_in> getVideoConversationClients();
	std::map<std::string, sockaddr_in> getAudioConversationClients();
	bool inConversation(std::string clientIp);
	void addToVideoConversation(std::pair<std::string, sockaddr_in> newParticipant);
	void addToAudioConversation(std::pair<std::string, sockaddr_in> newParticipant);
	void addToMsgLine(std::pair<std::string, std::string> newMsg);
	void addToChunkLine(std::pair<std::string, std::string> newChunk);
	void leaveConversation(std::string clientIp);

private:	
	// first - ip, second - buffer
	std::vector<std::pair<std::string, std::string>> messageLine;
	std::vector<std::pair<std::string, std::string>> chunkLine;

	// first - name and ID, second - ip.
	std::vector<std::pair<std::string, std::string>> conversationParticipants;

	// first - AES key, second - ip.
	std::vector<std::pair<AESCryptoAlgorithm, std::string>> participantsKeys;

	// key - ip, value - client socket.
	std::map<std::string, sockaddr_in> videoConversationClients;
	std::map<std::string, sockaddr_in> audioConversationClients;

	SOCKET videoSocket;
	SOCKET audioSocket;
	WSAInitializer wsaVideo;
	WSAInitializer wsaAudio;
	std::mutex videoMutex;
	std::mutex audioMutex;
	std::mutex messageLineMutex;
	std::mutex chunkLineMutex;
	std::mutex participantsMutex;

	std::string decrypt(std::string ip, std::string message);
	std::string encrypt(std::string ip, std::string message);
};