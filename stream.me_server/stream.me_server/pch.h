#pragma once

#pragma comment(lib, "Ws2_32.lib")

#include <iostream>
#include <string>
#include <map>
#include <thread>
#include <WinSock2.h>
#include <Windows.h>
#include <exception>
#include <thread>
#include <ws2tcpip.h>
#include <mutex>
#include <fstream>
#include <io.h>
#include <queue>
#include "json-develop/single_include/nlohmann/json.hpp"

#define MSG_CODE jsonMsg["code"]
#define PORT 3535
#define CHAT_PORT 5555
#define VIDEO_PORT 4242
#define AUDIO_PORT 3333
#define BUFFER_LENGTH 2048

//Message codes
#define EXIT "000"
#define SIGN_UP "001"
#define SIGN_IN "002"
#define GET_USERNAME "003"
#define SEND_FRIEND_REQUEST "004"
#define SHOW_FRIEND_REQUESTS "005"
#define RESPOND_FRIEND_REQUEST "006"
#define SHOW_FRIENDS "007"
#define REMOVE_FRIEND "008"
#define START_VIDEO_CONVERSATION "009"
#define JOIN_VIDEO_CONVERSATION "010"
#define HANG_UP "011"
#define OPEN_PRIVATE_CHAT "012"
#define SEND_PRIVATE_MSG "013"