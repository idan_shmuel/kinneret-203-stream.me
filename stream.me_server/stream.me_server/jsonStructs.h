#pragma once
#include "pch.h"

typedef struct SignUp
{
	std::string email;
	std::string username;
	std::string password;

}Signup;

typedef struct SignIn
{
	std::string email;
	std::string password;

}SignIn;

typedef struct FriendRequest
{
	std::string friend1;
	std::string ID1;
	std::string friend2;
	std::string ID2;

}FriendRequest;
